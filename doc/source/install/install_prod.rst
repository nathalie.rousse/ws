.. _install_prod:

=========================================
Installation procedure in production case
=========================================

Installation of ws in production case
=====================================

  .. literalinclude:: ../../../ws/install/prod/install.txt

install_singularity.txt :

  .. literalinclude:: ../../../ws/install/install_singularity.txt

install_muse.txt :

  .. literalinclude:: ../../../ws/install/prod/install_muse.txt

See : :ref:`Softwares installation <install_softwares>`

