.. _install_dev:

==========================================
Installation procedure in development case
==========================================

Installation of ws in development case
======================================

  .. literalinclude:: ../../../ws/install/dev/install.txt

install_singularity.txt :

  .. literalinclude:: ../../../ws/install/install_singularity.txt

install_muse.txt :

  .. literalinclude:: ../../../ws/install/dev/install_muse.txt

See : :ref:`Softwares installation <install_softwares>`

