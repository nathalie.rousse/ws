.. _api_get_admin:

=========
GET admin
=========

URL :: 

  http://127.0.0.1:8000/api/admin

Description
===========

SW database management.

Accessible only for Administrator.

