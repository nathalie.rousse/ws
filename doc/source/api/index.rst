.. _api:

===
API
===

.. include:: ui_token_right.rst

Ident and access
================

  - :ref:`api_get_software_list`

    :ref:`api_get_software_detail_name`

    :ref:`api_get_container_list`

  - :ref:`api_jwt` *(to get, verify)*

    :ref:`Access relation<api_access>` *(between users, softwares...)*

Run and get results
===================

  - :ref:`api_post_software_run_name` 

  - :ref:`api_post_software_muse_run_name` 

    :ref:`api_get_software_muse_state_name_key` 

    :ref:`api_get_software_muse_run_name_key` 

    :ref:`api_get_software_muse_content_name_key`

  - :ref:`api_get_download`

Admin
=====

  - :ref:`api_get_admin`

Specific tools
==============

  - :ref:`api_tools`

