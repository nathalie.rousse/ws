.. _api_tools:
  
==============
Specific tools
==============

- URL (POST request for a Tool) : ::

    http://127.0.0.1:8000/api/tool/...

- URL (GET request for a Tool User Interface) : ::

    http://127.0.0.1:8000/api/ui/...

Access
======

:ref:`auth` is required or not, it depends on tools *(see also* :ref:`JWT<api_jwt>`).

.. include:: ui_token_right.rst

Description
===========

Some resources dedicated to some specific tools.

- Specific toulbar2 software : ::

    http://127.0.0.1:8000/api/ui/toulbar2
    http://127.0.0.1:8000/api/ui/toulbar2/muse

- Specific pytoulbar2 software : ::

    http://127.0.0.1:8000/api/ui/pytoulbar2
    http://127.0.0.1:8000/api/ui/pytoulbar2/muse

- Specific sudoku tool : ::

    http://127.0.0.1:8000/api/ui/sudoku
    http://127.0.0.1:8000/api/tool/sudoku
    http://127.0.0.1:8000/api/tool/sudoku/muse

    http://127.0.0.1:8000/api/ui/sudoku/tut
    http://127.0.0.1:8000/api/tool/sudoku/tut
    http://127.0.0.1:8000/api/tool/sudoku/tut/muse

- Specific visualsudoku tool : ::

    http://127.0.0.1:8000/api/ui/vsudoku
    http://127.0.0.1:8000/api/tool/vsudoku
    http://127.0.0.1:8000/api/tool/vsudoku/muse

