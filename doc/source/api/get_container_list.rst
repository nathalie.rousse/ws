.. _api_get_container_list:

==================
GET container/list
==================

URL :: 

  http://127.0.0.1:8000/api/container/list

Description
===========

  *Under construction*

Request parameters
==================

  *Under construction*

Request response
================

  *Under construction*
  
Example
=======

  Request : ::

    curl -H "Content-Type: application/x-www-form-urlencoded" http://127.0.0.1:8000/api/container/list/

  Response : ::

    ...

