.. _index:
  
.. _home_ws:

====================================
Welcome to ws *(under construction)*
====================================

.. warning::
   This is a **test demonstrator** of the **ws** web services.
   It is **temporary** on line, provided for tests, to make it possible to try
   ws web services, to show and help thinking how they may *or not* be useful.
   It is provided *as is*, without any warranty.

Project
=======

    The **ws** web services allow to run some softwares.

    Before to be able to be used by ws web services, a software has to
    previously be delivered and installed as required by ws. Then it can be
    run on different resources : local run on ws machine, remote run on Muse
    cluster.

    :ref:`About ws<ws_about>` |
    :ref:`Authentication<auth>` |
    :ref:`More about Softwares <softwares>`


For users
=========

    :ref:`api` | :ref:`Examples (into API)<api>` | :ref:`faqs_users`

For developers & admin
======================

    :ref:`install` | :ref:`admin` | :ref:`faqs_developers`

Frequently Asked Questions
==========================

    :ref:`For users <faqs_users>` |
    :ref:`About softwares <faqs_softwares>` |
    :ref:`For developers <faqs_developers>`

Around ws
=========

    :ref:`glossary`

Table of contents
=================

.. toctree::
   :maxdepth: 2

   about/index
   home_users
   home_developers
   faqs/index
   glossary
