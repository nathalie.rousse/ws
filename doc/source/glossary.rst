.. _glossary:

========
Glossary
========

.. glossary::
   :sorted:

   Django
   django
       Django is a Python web framework.
       See https://www.djangoproject.com .

       Django notions of project and application :

       - A django project corresponds with a web project dedicated to a web
         site. It is a collection of configuration and django
         applications for a particular web site.

       - A django application is a Python package that follows a certain
         convention. It provides some web application functions. Developing
         django applications is the way how code is shared and reused between
         django projects.

   Django application
   django application
       See :term:`django`

   Django project
   django project
       See :term:`django`

   Singularity
   singularity
       Singularity is a platform and computer program that performs
       containerization.
       See https://singularity.lbl.gov .

   Docker
   docker
       *Under construction*

   Sphinx
   sphinx
       Sphinx is a documentation generator which converts reStructuredText
       files into HTML websites and other formats including PDF, EPub and man.

   INRAE
       French National Research Institute for Agriculture, Food and Environment
       See http://www.inrae.fr .

