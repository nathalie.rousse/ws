.. _faqs_softwares:

====================
FAQs about softwares
====================

Can ws web services be used for any software ?
==============================================

  - The software will have to be previously installed as required by ws.
    See :ref:`Softwares installation <install_softwares>`

  - The software has to follow **some rules** : *Under construction*

How is delivered a software ?
=============================

  - See :ref:`Softwares delivery <install_softwares>`

Is everybody allowed to use my software if I deliver it to ws ?
=============================================================

  - It depends on you : when you deliver your software to ws, you decide who
    will be allowed to use it, and with which level access.

