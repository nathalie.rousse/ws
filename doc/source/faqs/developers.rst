.. _faqs_developers:

===============
Developers FAQs
===============

Where can I find help as developer ?
------------------------------------

  - How to install, deploy the ws web services : :ref:`install`

  - How to install and register a software :
    :ref:`Softwares installation <install_softwares>`

  - How to build and install documentation : :ref:`install` (*search
    'documentation'*)

