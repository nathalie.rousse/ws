.. _faqs_users:

==========
Users FAQs
==========

How to call/use the ws web services ?
=====================================

    As RESTful web services, the ws web services are based on the HTTP
    protocol (a call takes the form of an HTTP request). They can be called
    from 'anything' that is able to send an HTTP request, for example : from
    a software program written in a language supporting the HTTP protocol
    (Python, R, C++, Java, Php...), from command line tools (cURL...), from
    a webbrowser...

    See :ref:`api` (examples...)

Can I use any software exposed by the ws web services ?
=======================================================

Can anyone use any software ?
-----------------------------

  - No. Authentication is required to run a software.

    See :ref:`auth`

How to authenticate into ws ?
-----------------------------

What is JSON Web Token authentication ?
---------------------------------------

How to get a token (JSON Web Token) ?
-------------------------------------

I would like to run a software. Do I have to register myself into ws ?
----------------------------------------------------------------------

  - See :ref:`auth`

Can I use my software running results later on ?
================================================

    - Yes. *Under construction*

How can I share my software running results with someone else ?
===============================================================

    - *Under construction*

Can I run a software with my own input datas files ?
====================================================

    - Yes. *Under construction*

