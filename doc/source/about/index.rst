.. _ws_about:

======
About
======


.. _ws_ip:

Intellectual property
=====================

:ref:`AUTHORS` | :ref:`LICENSE`

.. literalinclude:: WS


.. _ws_code:

Source code
===========

- Repository : https://forgemia.inra.fr/nathalie.rousse/ws.git

- :ref:`Source code API <apiref_index>`.


.. _ws_contacts:
  
Contacts
========

  You may find answers to your own questions in :ref:`faqs`.

  Issues : *Under construction*

