.. _softwares:

=========
Softwares
=========

.. _delivery_softwares:

Delivery
========

 .. literalinclude:: ../../../softwares/deliv/delivery.txt

.. _install_softwares:

Installation
============

 .. literalinclude:: ../../../softwares/install/install.txt

install_softwares.txt :

 .. literalinclude:: ../../../softwares/install/install_softwares.txt

install_softwares_documentation.txt :

 .. literalinclude:: ../../../softwares/install/install_softwares_documentation.txt

