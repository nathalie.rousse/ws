
POST_run.odg, POST_muse_run.odg and POST_muse_run.odt are source files to
generate the online files POST_run.pdf and POST_muse_run.pdf.

File POST_run.odg
=================

POST_run.odg to produce POST_run.pdf

From POST_run.odg to POST_run.pdf :

  - Modify POST_run.odg
  - Generate POST_run.pdf from POST_run.odg
  - mv POST_run.pdf ../source/api/files/POST_run.pdf



Files POST_muse_run.odg and POST_muse_run.odt
=============================================

POST_muse_run.odg and POST_muse_run.odt to produce POST_muse_run.pdf

From POST_muse_run.odg and POST_muse_run.odt to POST_muse_run.pdf :

  - Modify POST_muse_run.odg
  - Update POST_muse_run.odt by inserting modified POST_muse_run.odg
  - Generate POST_muse_run.pdf from POST_muse_run.odt
  - mv POST_muse_run.pdf ../source/api/files/POST_muse_run.pdf

