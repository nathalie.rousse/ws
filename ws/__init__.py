"""ws package

*This package is part of ws web services*

:copyright: Copyright (C) 2020-2024 INRAE http://www.inrae.fr
:license: GPLv3, see LICENSE file for more details
:authors: see AUTHORS file

The ws package contains the production (source code...).

The code is written in python language with the django web framework.

Content hierarchy : apps, projects, install...

"""

