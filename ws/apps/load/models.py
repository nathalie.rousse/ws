"""load.models
  
Models of the load application

"""

import os
import datetime

from django.db import models
from django.db.models.signals import pre_delete
from django.utils.deconstruct import deconstructible

from django.core.files import File
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from conf.config import DOWNLOADS_HOME_DEFAULT
from conf.config import DOWNLOAD_LIFETIME

from cmn.utils.folder_file import get_available_pathname
from cmn.utils.folder_file import delete_path_if_present

@deconstructible
class getDownloadToPath(object):
    def __call__(self, instance, filename) :
        return instance.get_available_filepath(
                                root=os.path.join(settings.CLOSEDMEDIA_ROOT,
                                                  instance.dirname),
                                date=instance.date, filename=filename)

get_download_to_path = getDownloadToPath()

class DownloadDocument(models.Model):
    """Downloadable document"""

    date = models.DateTimeField(auto_now_add=True)
    fs = FileSystemStorage(location=settings.CLOSEDMEDIA_ROOT,
                           base_url=settings.CLOSEDMEDIA_URL)
    docfile = models.FileField(upload_to=get_download_to_path, storage=fs,
                               max_length=600)
    key = models.CharField(max_length=60, blank=True,
                           help_text="download file key")
    dirname = models.CharField(max_length=50, blank=True,
          help_text="subdirectory name (under"+settings.CLOSEDMEDIA_ROOT+")")

    def is_old(self):
        """DownloadDocument is old if date 'older' than DOWNLOAD_LIFETIME """

        return self.is_old_indays(days=DOWNLOAD_LIFETIME)

    def delete_if_old(self):
        """Deletes DownloadDocument and its associated folder, if too old """

        if self.is_old() :
            self.delete()

    @classmethod
    def create(cls, file_path, dirname=DOWNLOADS_HOME_DEFAULT):
        """Creates a DownloadDocument for file file_path and dirname"""

        f = File(open(file_path, "rb"))
        document = cls()
        document.dirname = dirname
        document.save()
        document.docfile.save(name=os.path.basename(f.name), content=f,
                              save=True)
        document.key = document.def_key_value()
        document.save()
        return document

    def __str__(self):
        return "Download Document id "+str(self.id)

    @classmethod
    def get_id(cls, key):
        """Finds and returns the id of the object with key, or else None """

        for document in DownloadDocument.objects.all() :
            if document.key == key :
                return document.id
        return None

    @classmethod
    def get_available_filepath(cls, root, date, filename):
        """Defines and returns a new file path name

           The file path name is based on the input information,
           does not yet exist ...and should be 'immediately' created !
        """

        base = '%s_' % date.strftime("%Y%m%d_%H%M%S")
        dir = get_available_pathname(rootpath=root, rootname=base)
        dir = os.path.join(dir, filename)
        return dir

    def get_url(self):
        """Returns docfile path (relative) """

        return self.docfile.url

    def get_absolute_url(self):
        """Returns docfile absolute path """

        path = self.get_url()
        absolute_url = os.path.join(settings.CLOSEDMEDIA_ROOT, path)
        return absolute_url

    def def_key_value(self):
        """Returns key value after having defined it from url """

        return os.path.basename(os.path.dirname(self.get_url()))

    def clear_dir(self):
        """Deletes (if exists) the folder relative to docfile"""

        absolute_dir = os.path.dirname(self.get_absolute_url())
        delete_path_if_present(path=absolute_dir)

    def is_old_indays(self, days):
        """DownloadDocument is old if date 'older' than days """

        d = self.date + datetime.timedelta(days=days)
        now = datetime.datetime.now()
        if d.toordinal() < now.toordinal() :
           return True
        else :
           return False

def downloaddocument_clear(sender, instance, *args, **kwargs):
    """Deletes DownloadDocument associated folder"""

    instance.clear_dir()

# done before a DownloadDocument deletion
pre_delete.connect(downloaddocument_clear, sender=DownloadDocument)

