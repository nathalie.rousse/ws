"""load.admin"""

from django.contrib import admin

from load.models import DownloadDocument

downloaddocument_general_description = \
    "DownloadDocument containing responses contents"

def delete_too_old_downloaddocument(modeladmin, request, queryset):
    for downloaddocument in queryset :
        downloaddocument.delete_if_old()
delete_too_old_downloaddocument.short_description = \
    "Delete selected downloaddocuments if too old"

class DownloadDocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', 'dirname',)
    actions = [delete_too_old_downloaddocument]
    fieldsets = (
        (None, {
            'description': downloaddocument_general_description,
            'fields': ('docfile', 'key', 'dirname',), # not 'date'
        }),
    )

admin.site.register(DownloadDocument, DownloadDocumentAdmin)


