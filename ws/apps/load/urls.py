"""load.urls
  
Urls of the load application

"""

from django.urls import path

from load import views as load_views

urlpatterns = [

    # download
    path('download/', load_views.DownloadDocumentDetail.as_view(),
         name='load-download' ),

]

