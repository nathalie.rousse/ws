"""load.serializers """

from rest_framework import serializers

class KeyOptionSerializer(serializers.Serializer):
    """Option 'key' (for identification in downloading) """

    key = serializers.CharField(required=False)
    def validate(self, attrs):
        if 'key' not in attrs.keys() :
            attrs['key']=None
        return attrs

