"""load.views"""

from rest_framework import generics

from load.models import DownloadDocument

from load.views_mixins import KeyViewMixin
from load.views_mixins import DownloadViewMixin

headsubtitle = "(load)"

class DownloadDocumentDetail(KeyViewMixin, DownloadViewMixin,
                             generics.RetrieveAPIView):
    """One Download document """

    def get(self, request, format=None, **kwargs):
        """Returns DownloadDocument docfile content

        DownloadDocument is identified by 'key' option, not by id

        DownloadDocument is accessible by who knows its key (ie the user
        who made the request to build it).
        """

        try :
            data = request.query_params
            key = self.get_key_value(data=data)
            if key is None :
                errormsg = "Bad request : option key required"
                raise Exception(errormsg)

            download_documents = DownloadDocument.objects.all().filter(key=key)
            number_documents = len(download_documents)
            if number_documents < 1:
                errormsg = "Bad request : nothing to download with that key="
                errormsg = errormsg + key
                raise Exception(errormsg)
            elif number_documents > 1:
                errormsg = "Bad request : problem with that key="
                errormsg = errormsg + key
                errormsg = errormsg + " too many candidates for that key"
                raise Exception(errormsg)
            download_document = download_documents[0] # the only one
            path = download_document.get_url()
            response = self.file_response(file_path=path)

        except :
            raise

        return response

