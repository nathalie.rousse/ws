"""load.views_mixins

Mixins common to views using management provided by load application

"""

from rest_framework.reverse import reverse
from rest_framework.response import Response

from cmn.utils.urls import url_add_options

from load.serializers import KeyOptionSerializer
from load.models import DownloadDocument

from cmn.views_mixins import FileViewMixin

class DownloadViewMixin(FileViewMixin):
    """Additional methods for views with downloadable results """

    def build_download_folder(self, file_path, dirname):
        """Builds and returns a downloadable folder DownloadDocument
           containing the file found at file_path
        """

        download_document = DownloadDocument.create(file_path=file_path,
                                                    dirname=dirname)
        return download_document

    def get_download_base_url(self, request) :
        return reverse('load-download', request=request)

    def get_download_url(self, base_url, download_document):
        """Returns the url to download the download_document content """

        key = download_document.key

        url = base_url
        if "?" in url : # remaining option(s)
            url = url.split("?")[0]

        options = {'key':key}
        
        more_text = "The resource to download the response content is "
        more_text += "'GET download'"
        more_text += " with 'key' option (key value : " + key + ")"

        download_url = url_add_options(url, options)
        more_text += "\n"
        more_text += "Keep this key value in order to be able to download"
        more_text += " the response content later on : key=" + key

        return (download_url, more_text)

    def download_response(self, url, label=None, more_text=None):
        if url:
            if not label :
                label = "The downloading url"
            msg_text = label + " : " + url
            if more_text :
                text = more_text.replace('\n', ' *** ')
                msg_text = msg_text + " *** " + text
            context = {'msg':msg_text, "url":url}
        else :
            context = {'msg':"No downloadable result"}
        return Response(context)

class KeyViewMixin(object):
    """Additional methods for views having key option """

    def get_key_value(self, data):
        """Returns the key value found in data and else, None """

        p = KeyOptionSerializer(data=data)
        p.is_valid()
        return p.data['key']

