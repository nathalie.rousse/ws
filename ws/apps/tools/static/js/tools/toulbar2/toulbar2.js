
function download_bash_file(filename)
{ // to create and edit the bash file

  var pom = document.createElement('a');

  pom.setAttribute('href', 'data:text/plain;charset=utf-8,'
+ encodeURIComponent('#!/bin/bash\n\n')
+ encodeURIComponent('PATH=$PATH:/toulbar2/build/bin/Linux:/toulbar2/src\n')
+ encodeURIComponent('export PATH\n\n')
+ encodeURIComponent('# run\n')
+ encodeURIComponent('cd /WS\n')
+ encodeURIComponent('toulbar2 --help\n\n'));

  pom.setAttribute('download', filename);
  if (document.createEvent) {
      var event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
  } else { pom.click(); }
}

