
function download_python_file(filename)
{ // to create and edit the python file

  var pom = document.createElement('a');

  pom.setAttribute('href', 'data:text/plain;charset=utf-8,'
      + encodeURIComponent('from pytoulbar2 import CFN\n')
      + encodeURIComponent('import numpy\n\n')
      + encodeURIComponent('myCFN = CFN(1)\n\n')
      + encodeURIComponent('myCFN.Solve()\n\n')
      + encodeURIComponent('print("problem end OK")\n'));
  pom.setAttribute('download', filename);
  if (document.createEvent) {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  } else { pom.click(); }
}

function download_bash_file(filename)
{ // to create and edit the bash file

  var pom = document.createElement('a');

  pom.setAttribute('href', 'data:text/plain;charset=utf-8,'
   + encodeURIComponent('#!/bin/bash\n\n')
   + encodeURIComponent('PYTHONPATH=$PYTHONPATH:/WS:/WS/pythonplus\n')
   + encodeURIComponent('export PYTHONPATH\n\n')
   + encodeURIComponent('# add here specific required !!!\n')
   + encodeURIComponent('pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade numpy\n\n')
   + encodeURIComponent('# add here what to be run !!!\n')
   + encodeURIComponent('cd /WS\n')
   + encodeURIComponent('python3 problem.py\n\n')
   + encodeURIComponent('# clean\n')
   + encodeURIComponent('rm -fr /WS/pythonplus\n')
   + encodeURIComponent('rm -fr /WS/cache\n\n'));

  pom.setAttribute('download', filename);
  if (document.createEvent) {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  } else { pom.click(); }
}

