
// csrf_token

// note : var csrf_token_value must exist
document.cookie = "csrf_token=" + csrf_token_value + "";
document.getElementById("csrf_token").innerHTML = '<input name="csrf_token" value='+csrf_token_value+' />';

function toggle_visibility(id)
{ // to show/hide element id

  var e = document.getElementById(id);
  if (e!=null){
      if (e.style.display == 'block'){ e.style.display = 'none';
      } else { e.style.display = 'block'; }
  }
}

function force_visibility(id)
{ // to show element id

  var e = document.getElementById(id);
  if (e!=null){
      e.style.display = 'block';
  }
}

function download(filename, text)
{ // to create and edit filename file
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);
  if (document.createEvent) {
      var event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
  } else { pom.click(); }
}

