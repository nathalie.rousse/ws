#!/bin/bash

###############################################################################
# Source : "toulbar2 User documentation"
# https://miat.inrae.fr/toulbar2/documentation.html, User manual
###############################################################################

PYTHONPATH=$PYTHONPATH:/WS:/WS/pythonplus
export PYTHONPATH

# sudoku specific required
#already into pytoulbar2plus
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade matplotlib
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade pandas
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade nbconvert
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade numpy
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade more-itertools

# run
cd /WS
python3 sudoku_tut.py $1

# clean
rm -fr /WS/pythonplus
rm -fr /WS/cache

