"""tools.urls
  
Urls of the tools application

"""

from django.urls import path

from django.views.generic import TemplateView

#from sw.views import views as sw_views
from tools.views.sudoku import views as sudoku_views
from tools.views.visualsudoku import views as visualsudoku_views


urlpatterns = [

    # specific toulbar2 software

    path('ui/toulbar2',
         TemplateView.as_view(template_name='tools/toulbar2/toulbar2.html'),
                              name="ui-toulbar2"),

    path('ui/toulbar2/muse',
         TemplateView.as_view(
                            template_name='tools/toulbar2/toulbar2_muse.html'),
                            name="ui-pytoulbar2-muse"),

    # specific pytoulbar2 software

    path('ui/pytoulbar2',
         TemplateView.as_view(
                             template_name='tools/pytoulbar2/pytoulbar2.html'),
                             name="ui-pytoulbar2"),

    path('ui/pytoulbar2/muse',
         TemplateView.as_view(
                        template_name='tools/pytoulbar2/pytoulbar2_muse.html'),
                        name="ui-pytoulbar2-muse"),


    # specific sudoku (pytoulbar2plus software)

    path('ui/sudoku',
         TemplateView.as_view(template_name='tools/sudoku/sudoku.html'),
                              name="ui-sudoku"),
    path('tool/sudoku',
         sudoku_views.ToolSudokuRun.as_view(), name='t-sudoku' ),
    path('tool/sudoku/muse',
         sudoku_views.ToolSudokuMuseRun.as_view(),
         name='t-sudoku-muse' ),

    path('ui/sudoku/tut',
         TemplateView.as_view(template_name='tools/sudoku/sudoku_tut.html'),
                              name="ui-sudoku-tut"),
    path('tool/sudoku/tut',
         sudoku_views.ToolSudokuTutRun.as_view(), name='t-sudoku-tut' ),
    path('tool/sudoku/tut/muse',
         sudoku_views.ToolSudokuTutMuseRun.as_view(),
         name='t-sudoku-tut-muse' ),

    # specific visualsudoku (pytoulbar2plus software)

    path('ui/vsudoku',
         TemplateView.as_view(template_name='tools/visualsudoku/visualsudoku.html'),
                              name="ui-vsudoku"),

    path('tool/vsudoku',
         visualsudoku_views.ToolVisualsudokuRun.as_view(), name='t-vsudoku' ),
    path('tool/vsudoku/muse',
         visualsudoku_views.ToolVisualsudokuMuseRun.as_view(),
         name='t-vsudoku-muse' ),

]

