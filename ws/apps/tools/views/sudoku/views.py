"""tools.views.sudoku.views """

from django.urls import resolve
  
from rest_framework import generics
from rest_framework import serializers

from sw.models import Software
from sw.models_mixins.resource import MuseResource

from cmn.utils.errors import get_exception_info

from sw.views.views import SwRun
from sw.views.views import SwMuseRun
from sw.views.views_run_mixins import SwRunViewMixin

import os
from cmn.utils.folder_file import recursive_overwrite
from cmn.utils.folder_file import set_all_user_rights

from tools.config import TOOLS_PATH
TOOL_SUDOKU_SRC_PATH = os.path.join(TOOLS_PATH, "sudoku/src")

from cmn.utils.log import to_log

headsubtitle = "(tools-sudoku)"

class ToolRunViewMixin(SwRunViewMixin):

    def get_tool_case(self, request) :
        """tool_case used to distinguish sudoku and sudoku_tut calls

        Returns "sudoku_tut" or "sudoku"
        """

        url_name = resolve(request.path_info).url_name
        if url_name in ['t-sudoku-tut', 't-sudoku-tut-muse'] :
            tool_case = "sudoku_tut"
        else : # 't-sudoku', 't-sudoku-muse'
            tool_case = "sudoku"
        return tool_case

    def get_sw(self, request, **kwargs) :
        """gets and returns the Software with name=pytoulbar2plus """

        (error_case, errortype, errordetails, errormsg) = \
                                             (False, None, None, "") # default
        sw_name = "pytoulbar2plus"
        sw = None
        try :
            sw = self.get_object(name=sw_name, request=request)
        except Software.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "Software with "+sw_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)

        return (sw, error_case, errortype, errordetails, errormsg)

    def prepare_files(self, data, wk) :
        """Prepares files in that tool case (internal files)

        Called by POST of ToolSudoku(Tut)Run and ToolSudoku(Tut)MuseRun

        "tool_case" key has been added into data.
        data["tool_case"] available values :
        - "sudoku" for ToolSudokuRun and ToolSudokuMuseRun
        - "sudoku_tut" for ToolSudokuTutRun and ToolSudokuTutMuseRun
        """

        try :
            if data["tool_case"] == "sudoku_tut" :
                ordered_filename_list = ["sudoku_tut_cmd.sh", "sudoku_tut.py",
                                         "valid.csv.xz"]
            else : # "sudoku"
                ordered_filename_list = ["sudoku_cmd.sh", "sudoku.py",
                                         "valid.csv.xz"]
            for fname in ordered_filename_list :
                file_path = os.path.join(wk.run_path, fname)
                src_path = os.path.join(TOOL_SUDOKU_SRC_PATH, fname)
                recursive_overwrite(src=src_path, dest=file_path)
                set_all_user_rights(file_path) # file_path FORCED to 755 
        except :
            raise
        return ordered_filename_list

    def get_indice_value(self, data):
        """Returns the 'indice' value

        From parameter 'indice' or 'Indice', 'INDICE', 'ind', 'IND'

        Default value (if parameter not found) : 0
        """

        for key in ['indice', 'Indice', 'INDICE', 'ind', 'IND'] :
            if key in data.keys() :
                return int(data[key])
        return 0

    def specific_data(self, request, tool_case="sudoku") :
        """Adapts request data according to the tool

        Called by POST of ToolSudoku(Tut)Run and ToolSudoku(Tut)MuseRun

        tool_case available values :
        - "sudoku" for ToolSudokuRun and ToolSudokuMuseRun
        - "sudoku_tut" for ToolSudokuTutRun and ToolSudokuTutMuseRun

        Note : 'sbatch_list' and 'sbatch_list' don't concern
        ToolSudoku(Tut)Run but don't bother
        """

        tool_case = self.get_tool_case(request)

        # overides request data according to the tool
        self.request_data_writable(request)
        data = self.request_data(request)

        #print("REQUETE avant modif -- data : ", data)

        data['tool_case'] = tool_case

        # new (specific) input parameter

        indice = self.get_indice_value(data)

        if tool_case == "sudoku_tut" :
            data['cmd'] = "/bin/bash sudoku_tut_cmd.sh "+str(indice) # overriden
        else : # "sudoku"
            data['cmd'] = "/bin/bash sudoku_cmd.sh "+str(indice) # overriden

        if 'cmd_option' in data.keys() :
            del data['cmd_option'] # forbidden

        #data['returned_type'] : unchanged

        #data['todownload']    : unchanged

        # Note : see
        # prepare_files for input files sudoku_cmd.sh, sudoku.py, valid.csv.xz
        if 'file' in data.keys() :
            del data['file'] # forbidden (not necessary for that tool)
        if 'files_order' in data.keys() :
            del data['files_order'] # forbidden (not necessary for that tool)

        if 'sbatch_list' in data.keys() :
            del data['sbatch_list'] # forbidden
        if 'module_list' in data.keys() :
            del data['module_list'] # forbidden

        #print("REQUETE apres modif -- data : ", data)

        self.request_data_unwritable(request)

class ToolSudokuRun(ToolRunViewMixin, SwRun):
    """SwRun specialized for sudoku tool """

# => pour essai :
# curl --output sudoku_run.zip -F 'returned_type=run.zip' -F 'todownload=no' http://127.0.0.1:8000/api/tool/sudoku

    def post(self, request, **kwargs):

        self.specific_data(request=request)

        #print("ToolSudokuRun ordre heritage : ", ToolSudokuRun.__mro__)

        return self.post_local_run(request=request, AUTH_REQUIRED=False,
                                   **kwargs)

class ToolSudokuMuseRun(ToolRunViewMixin, SwMuseRun):
    """SwMuseRun specialized for sudoku tool """

    def post(self, request, **kwargs):
        rsrc = MuseResource()
        self.specific_data(request=request)
        return self.post_remote_run(request=request, rsrc=rsrc,
                                    AUTH_REQUIRED=False, **kwargs)

class ToolSudokuTutRun(ToolRunViewMixin, SwRun):
    """SwRun specialized for sudoku_tut tool """

    def post(self, request, **kwargs):
        self.specific_data(request)
        return self.post_local_run(request=request, AUTH_REQUIRED=False,
                                   **kwargs)

class ToolSudokuTutMuseRun(ToolRunViewMixin, SwMuseRun):
    """SwMuseRun specialized for sudoku_tut tool """

    def post(self, request, **kwargs):
        rsrc = MuseResource()
        self.specific_data(request)
        return self.post_remote_run(request=request, rsrc=rsrc,
                                    AUTH_REQUIRED=False, **kwargs)

