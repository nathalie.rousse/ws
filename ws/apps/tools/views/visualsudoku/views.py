"""tools.views.visualsudoku.views """

import os

from django.urls import resolve
  
from rest_framework import generics
from rest_framework import serializers

from sw.models import Software
from sw.models_mixins.resource import MuseResource
from sw.models_mixins.workspace import RunWorkspace

from cmn.utils.errors import get_exception_info

from sw.views.views import SwRun
from sw.views.views import SwMuseRun
from sw.views.views_run_mixins import SwRunViewMixin

import os
from cmn.utils.folder_file import recursive_overwrite
from cmn.utils.folder_file import save_uploaded_file
from cmn.utils.folder_file import is_zip_file
from cmn.utils.folder_file import set_all_user_rights


from tools.config import TOOLS_PATH
TOOL_VISUALSUDOKU_SRC_PATH = os.path.join(TOOLS_PATH, "visualsudoku/src")

from cmn.utils.log import to_log

headsubtitle = "(tools-visualsudoku)"

class ToolRunViewMixin(SwRunViewMixin):

    def get_sw(self, request, **kwargs) :
        """gets and returns the Software with name=pytoulbar2plus """

        (error_case, errortype, errordetails, errormsg) = \
                                             (False, None, None, "") # default
        sw_name = "pytoulbar2plus"
        sw = None
        try :
            sw = self.get_object(name=sw_name, request=request)
        except Software.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "Software with "+sw_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)

        return (sw, error_case, errortype, errordetails, errormsg)

    def prepare_files(self, data, wk) :
        """Prepares files in that tool case (internal files + only 1)

        Called by POST of ToolVisualsudokuRun and ToolVisualsudokuMuseRun
        
        1 and only 1 input file is required : image file (partial grid)

        """

        try :
            ordered_filename_list = ["visualsudoku_cmd.sh",
                                     "mixed_classifier.h5",
                                     "puzzle.py",
                                     "toulbar2_visual_sudoku_puzzle.py"]

            for fname in ordered_filename_list :
                file_path = os.path.join(wk.run_path, fname)
                src_path = os.path.join(TOOL_VISUALSUDOKU_SRC_PATH, fname)
                recursive_overwrite(src=src_path, dest=file_path)
                set_all_user_rights(file_path) # file_path FORCED to 755 

            errormsg_missing = "Error input file missing ;"
            errormsg_missing += " partial grid expected as an image file"
            if 'file' in data.keys() :

                files = self.get_files(data)
                if len(files) < 1 :
                    raise Exception(errormsg_missing)

                elif len(files) > 1 :
                    errormsg = "Error to many input files given, only 1 is"
                    errormsg += " required (image file of partial grid)"
                    raise Exception(errormsg)

                else : # len(files) == 1

                    f = files[0]
                    file_path = os.path.join(wk.run_path, f.name)
                    cr_ok = save_uploaded_file(imulf=f, file_path=file_path)
                    set_all_user_rights(file_path) # __wsdev__ Careful :
                                                   # file_path FORCED to 755 
                    if not cr_ok :
                        errormsg = "Error while saving uploaded file named "
                        errormsg += f.name
                        raise Exception(errormsg)

                    if is_zip_file(file_path=file_path)  :
                        errormsg = "Error zip received as input file "
                        errormsg += " " + f.name + " ;"
                        errormsg += " partial grid expected as an image file"
                        raise Exception(errormsg)

                    ordered_filename_list.append(f.name)
            else :
                raise Exception(errormsg_missing)

        except :
            raise
        return ordered_filename_list

    def get_keep_value(self, data):
        default_value = 0
        for key in ['k', 'keep'] :
            if key in data.keys() :
                v = int(data[key])
                if v >= 0 :
                    return v
        return default_value

    def get_border_value(self, data):
        default_value = 0
        for key in ['b', 'border'] :
            if key in data.keys() :
                v = int(data[key])
                if v >= 0 :
                    return v
        return default_value

    def get_time_value(self, data):
        default_value = 5
        vmax = 60
        for key in ['t', 'time'] :
            if key in data.keys() :
                v = int(data[key])
                if v < 0 : # refused => default value
                    v = default_value
                elif v > vmax :
                    v = vmax
                return v
        return default_value

    def get_image_file_name(self, data):
        """ Returns name of the image file input (default value if problem) """

        image_file_name = "undefinedimagefile" # default
        if 'file' in data.keys() :
            files = self.get_files(data)
            if len(files) == 1 :
                f = files[0]
                image_file_name = f.name
        return image_file_name

    def specific_data(self, request) :
        """Adapts request data according to the tool

        Called by POST of ToolVisualsudokuRun and ToolVisualsudokuMuseRun

        Note : 'sbatch_list' and 'sbatch_list' don't concern
        ToolVisualsudokuRun but don't bother

        How are treated parameters of tool toulbar2_visual_sudoku_puzzle.py :

          - -m --model : path to trained digit classifier ; required
               --> fixed into visualsudoku_cmd.sh
          - -i --image : path to input Sudoku puzzle image ; required
               --> 'file' request parameter
          - -o --output : path to ouput Sudoku solution image ; required
               --> fixed into visualsudoku_cmd.sh
          - -d --debug : whether or not we are visualizing each step of the
               pipeline ; type=int, default=-1
               --> ignored
          - -k --keep" : keep percentage of the center of cell images without
               removing grid lines ; type=int, default=0
               --> 'keep' request parameter
          - -b --border : enlarge the cell image region by some extra
               percentage ; type=int, default=0
               --> 'border' request parameter
          - -t --time : CPU time limit in seconds for solving sudoku ;
               type=int, default=5, maxvalue=60
               --> 'time' request parameter
        """

        # overides request data according to the tool
        self.request_data_writable(request)
        data = self.request_data(request)

        image = self.get_image_file_name(data)
        keep = self.get_keep_value(data)
        border = self.get_border_value(data)
        time = self.get_time_value(data)

        # Note : image file input captured into prepare_files
        # See prepare_files also for input files of the tool : 
        # visualsudoku_cmd.sh, toulbar2_visual_sudoku_puzzle.py ...

        if 'cmd_option' in data.keys() :
            del data['cmd_option'] # forbidden

        #data['returned_type'] : unchanged 
        # Attention : the 'stdout' value (default value) has here a new
        # signification : means to return "solution.jpg" image file

        #data['todownload']    : unchanged

        if 'files_order' in data.keys() :
            del data['files_order'] # forbidden (not necessary for that tool)

        if 'sbatch_list' in data.keys() :
            del data['sbatch_list'] # forbidden
        if 'module_list' in data.keys() :
            del data['module_list'] # forbidden

        cmd = "/bin/bash visualsudoku_cmd.sh "+ str(image) 
        cmd += " "+ str(keep) +" "+ str(border) +" "+ str(time)
        data['cmd'] = cmd # overriden

        #print("REQUETE apres modif -- data : ", data)

        self.request_data_unwritable(request)

    def build_response(self, base_url, returned_type, todownload, run_path,
                       report) :
        """Builds response in that tool case

        If returned_type='stdout.txt' or 'run.zip' then response as usual,
        Else (returned_type='stdout' or anything else) :
        response "solution.jpg" image file.
        """

        def make_download_response(file_path, dirname) :
            download_document = self.build_download_folder(
                                         file_path=file_path, dirname=dirname)
            label = "The response content can be downloaded at"
            (download_url, more_text) = self.get_download_url(
                                          base_url=base_url,
                                          download_document=download_document)
            response = self.download_response(url=download_url, label=label,
                                              more_text=more_text)
            return response

        rwk = RunWorkspace.get_run_workspace(run_path=run_path)
        if returned_type == "run.zip" :
            zip_folder_path = rwk.make_run_zip(report=report)
            if todownload :
                response = make_download_response(file_path=zip_folder_path,
                                                  dirname=DOWNLOADS_HOME_SW)
            else :
                response = self.zipfile_response(file_path=zip_folder_path)

        elif returned_type == "stdout.txt" :
            stdout_file_path = rwk.get_stdout_file_path()
            if todownload :
                response = make_download_response(file_path=stdout_file_path,
                                                  dirname=DOWNLOADS_HOME_SW)
            else :
                response = self.txtfile_response(file_path=stdout_file_path)

        else : # returned_type=="stdout", else
            # specific

            # Attention : image file name "solution.jpg" must be the same as
            # into visualsudoku_cmd.sh

            image_file_path = os.path.join(run_path, "solution.jpg")
            response = self.binaryfile_response(file_path=image_file_path)

        return response

class ToolVisualsudokuRun(ToolRunViewMixin, SwRun):
    """SwRun specialized for visualsudoku tool """

# => pour essai :
# curl --output myRUN.zip -F 'file=@MYGRID.jpg' -F 'returned_type=run.zip' -F 'k=40' -F 'b=15' -F 't=10' http://127.0.0.1:8000/api/tool/vsudoku
# curl -F 'file=@MYGRID.jpg' -F 'returned_type=stdout.txt' -F 'k=40' -F 'b=15' -F 't=10' http://127.0.0.1:8000/api/tool/vsudoku
# curl --output mysolution.jpg -F 'file=@MYGRID.jpg' -F 'k=40' -F 'b=15' -F 't=10' http://127.0.0.1:8000/api/tool/vsudoku
# Note : il y a aussi parameter todownload

    def post(self, request, **kwargs):

        try :
            self.specific_data(request=request)
        except :
            raise

        #print("ToolVisualsudokuRun ordre heritage : ", ToolVisualsudokuRun.__mro__)

        return self.post_local_run(request=request, AUTH_REQUIRED=False,
                                   **kwargs)

class ToolVisualsudokuMuseRun(ToolRunViewMixin, SwMuseRun):
    """SwMuseRun specialized for sudoku tool """

    def post(self, request, **kwargs):
        rsrc = MuseResource()
        self.specific_data(request=request)
        return self.post_remote_run(request=request, rsrc=rsrc,
                                    AUTH_REQUIRED=False, **kwargs)

