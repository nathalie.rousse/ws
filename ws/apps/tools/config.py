"""tools.config

Contains the tools configuration : paths, constants...

"""

import os

from conf.config import PACKAGE_HOME

TEMPLATE_TOOLS = os.path.join(PACKAGE_HOME, 'apps', 'tools', 'templates')
"""templates of tools app"""

TOOLS_PATH = os.path.join(PACKAGE_HOME, 'apps', 'tools', 'tools')
"""embedded tools folder"""
