"""acs.models
  
Models of the acs application

"""

from django.db import models
from django.contrib.auth.models import User

from sw.models import OContainer


ht_acs_text = "free text"
ht_acs_usr = "the User having the access"
ht_acs_ocn = "the accessed OContainer"
ht_acs_level = "the level value (in 1,2,3,4) defining rights"

class Access(models.Model):
    """Access defining a level of access for one User to one OContainer.

    Attributes :

    - text
    - usr : relative User (many Access to one User relationship)
    - ocn : relative OContainer (many Access to one OContainer relationship)
    - level : value (in 1,2,3,4) defining rights
    """

    text = models.CharField(max_length=2000, blank=True, default=None,
                            help_text=ht_acs_text)

    usr = models.ForeignKey(User, on_delete=models.CASCADE,
                            related_name="access_list",
                            verbose_name="related User", blank=False,
                            help_text=ht_acs_usr)

    ocn = models.ForeignKey(OContainer, on_delete=models.CASCADE,
                            related_name="access_list",
                            verbose_name="related OContainer", blank=False,
                            help_text=ht_acs_ocn)

    level = models.IntegerField(blank=True, default=1, help_text=ht_acs_level)

    # unused
    @classmethod
    def get_usr_name_list(cls, ocn, level=None) :
        """Returns Users having access to the OContainer

        Returns list of names of users (User) with access to ocn (OContainer)

        Any level access in level=None case, level access if level in 1,2,3,4.
        """

        usr_name_list = list()
        try :
            if level in (1, 2, 3, 4) :
                for ocn_access in ocn.access_list.all().filter(level=level) :
                    usr_name_list.append(ocn_access.usr.username)
            else : # None or else => all levels
                for ocn_access in ocn.access_list.all() :
                    usr_name_list.append(ocn_access.usr.username)
        except :
            raise
        return usr_name_list

    # unused
    @classmethod
    def get_ocn_name_list(cls, usr, level=None) :
        """Returns OContainers to which the User has access

        Returns list of names of containers (OContainer) for which usr (User)
        has an access.

        Any level access in level=None case, level access if level in 1,2,3,4.
        """

        ocn_name_list = list()
        try :
            if level in (1, 2, 3, 4) :
                for usr_access in usr.access_list.all().filter(level=level) :
                    ocn_name_list.append(usr_access.ocn.name)
            else : # None or else => all levels
                for usr_access in usr.access_list.all() :
                    ocn_name_list.append(usr_access.ocn.name)
        except :
            raise
        return ocn_name_list

