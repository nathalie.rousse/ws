"""acs.forms"""

from django import forms

from acs.models import Access

from acs.models import ht_acs_level


class AccessForm(forms.ModelForm):

    level = forms.IntegerField(initial=1, max_value=4, min_value=1,
                               required=True, help_text=ht_acs_level)


    def __init__(self, *args, **kwargs):
        super(AccessForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n == "text" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':50})

    class Meta:
        model = Access
        fields = '__all__'

