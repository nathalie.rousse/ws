"""acs.views.views_mixins

Mixins for acs application views, and views of other applications using acs

"""

from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import permissions

from cmn.serializers import JwtSerializer

from cmn.views_mixins import DataViewMixin

from cmn.utils.errors import get_exception_info
from cmn.utils.errors import Http403


class AccessViewMixin(object):
    """Additional methods for Access views"""

    pass


class AccessPermission(permissions.BasePermission):
    """Methods about access permission rules """

    def has_object_permission(self, request, view, obj):
        data = view.data_from_request(request)
        access_ok = view.access_ok(model=obj, data=data)
        return access_ok


class AccessibilityViewMixin(BaseJSONWebTokenAuthentication, DataViewMixin):
    """Additional methods for views in limited access

    JSON Web Token Authentication scheme.
    The JWT (JSON Web Token) comes in the querystring. 
    """

    permission_classes = (AccessPermission,)

    def get_jwt_value(self, data):
        """Returns the JWT value found in data and else, None """
  
        p = JwtSerializer(data=data)
        p.is_valid()
        return p.data['jwt']

    def get_authenticated_user(self, data):
        """Returns the authenticated User

        Returns user if token OK, None if no token found,
        else fails (exception) : if token not OK...
        """

        user = None # default
        res = self.authenticate(data)
        if res is not None :
            (user, jwt_value) = res
        return user

    def control_access(self, ocn, request):
        """Returns user_name if ocn accessible by user authenticated into data

        If a user has been authenticated and is authorized for the OContainer \
        ocn then returns user_name and else sends an exception.

        Also returned : level access (of user_name for ocn), from which
        treatments will depend on.

        """

        ocn_name = ocn.name
        data = self.request_data(request)
        user_name = None # default
        level = None # default

        try :
            user = self.get_authenticated_user(data=data)

        except Exception as e :
            errormsg = "Error while control access : "
            (errortype, errordetails) = get_exception_info(e)
            if isinstance(e, AuthenticationFailed) :
                errormsg = errormsg + " (AuthenticationFailed) -- "
            errormsg = errormsg + errortype + " -- "
            errormsg = errormsg + str(errordetails) + " -- "
            raise Http403(errormsg)

        if user is None :
            errormsg = "Error while control access : "
            errormsg = errormsg + "a token is required (option jwt=...)"
            raise Http403(errormsg)

        user_name = user.username
        for usr_access in user.access_list.all() :
            if usr_access.ocn.name == ocn_name :
                level = usr_access.level

        # ws_072021
        # on peut distinguer dans boucle ci-dessus
        # cas ou ocn not found de cas ou mauvais level

        if level not in (1, 2, 3, 4) :
            errormsg = "Error while control access : "
            errormsg = errormsg + "bad token, "
            errormsg = errormsg + "unauthorized user (name " +user_name+ ")"
            raise Http403(errormsg)

        return (user_name, level)

    def access_level_max_value(self) :
        """Returns the max value of access level"""

        return 4

    def access_files_allowed(self, level) :
        """Returns if according to level, sending file is allowed or not

        Corresponds with 'files' and 'files_order' request parameters.

        Rights depend on level value (see acs.admin description) :

          1 (visitor) < 2 (user) < 3 (superuser) < 4 (admin)

          ---------------------------------------------
                        level |  1  |  2  |  3  |  4  |
          ---------------------------------------------
           files, files_order |     |  X  |  X  |  X  |
          ---------------------------------------------

        """

        if level <= 1 :
            return False
        else :
            return True

    def access_cluster_conf_allowed(self, level) :
        """Returns if according to level, configuring cluster is allowed or not

        Corresponds with 'sbatch_list' and 'module_list' request parameters.

        Rights depend on level value (see acs.admin description) :

          1 (visitor) < 2 (user) < 3 (superuser) < 4 (admin)

          ---------------------------------------------------
                              level |  1  |  2  |  3  |  4  |
          ---------------------------------------------------
           sbatch_list, module_list |     |     |  X  |  X  |
          ---------------------------------------------------

        """

        if level <= 2 :
            return False
        else :
            return True

