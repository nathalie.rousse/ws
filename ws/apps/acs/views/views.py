"""acs.views.views """

from rest_framework.response import Response
from rest_framework import generics
###from rest_framework import serializers

from django.contrib.auth.models import User

from sw.models import OContainer
from sw.models import Software

from acs.views.views_mixins import AccessViewMixin

#from cmn.utils.log import to_log
from cmn.utils.errors import complete_error_message
from cmn.utils.errors import get_exception_info

headsubtitle = "(acs)"

############################### OContainer #################################### 

class OContainerAccessUserNameListView(AccessViewMixin,
                                       generics.RetrieveAPIView):
    """Returns the Users having access to the OContainer

    Returns the list of the names of users (User) with an access (any level)
    to the container (OContainer) named 'name'
    """

    def get_queryset(self):
        return OContainer.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        ocn_name = kwargs['name']
        try : 
            ocn = OContainer.objects.get(name=ocn_name)
        except OContainer.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "OContainer with "+ocn_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                username_list = list()
                for ocn_access in ocn.access_list.all() :
                    username_list.append(ocn_access.usr.username)
                context = username_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class OContainerAccessLevelUserNameListView(AccessViewMixin,
                                            generics.RetrieveAPIView):
    """Returns the Users having 'level' access to the OContainer

    Returns the list of the names of users (User) with an access ('level'
    level) to the container (OContainer) named 'name'
    """

    def get_queryset(self):
        return OContainer.objects.all()
                                       
    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        ocn_name = kwargs['name']
        level = kwargs['level']
        if (level <= 0) or (level > 4)  :
            error_case = True
            errormsg = "Unavailable 'level' value "+str(level)
            errordetails = "Available 'level' values : 1,2,3,4"
        if not error_case :
            try : 
                ocn = OContainer.objects.get(name=ocn_name)
            except OContainer.DoesNotExist as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
                errormsg = "OContainer with "+ocn_name+" as name does not exist"
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                username_list = list()
                for ocn_access in ocn.access_list.all().filter(level=level) :
                    username_list.append(ocn_access.usr.username)
                context = username_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class UserAccessOContainerNameListView(AccessViewMixin,
                                       generics.RetrieveAPIView):
    """Returns the OContainers to which the User has access

    Returns the list of the names of containers (OContainer) for which the
    user (User) named 'name' has an access (any level)
    """

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        usr_name = kwargs['name']
        try : 
            usr = User.objects.get(username=usr_name)
        except User.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "User with "+usr_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                ocnname_list = list()
                for usr_access in usr.access_list.all() :
                    ocnname_list.append(usr_access.ocn.name)
                context = ocnname_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class UserAccessLevelOContainerNameListView(AccessViewMixin,
                                            generics.RetrieveAPIView):
    """Returns the OContainers to which the User has 'level' access

    Returns the list of the names of containers (OContainer) for which the
    user (User) named 'name' has an access ('level' level)
    """

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        usr_name = kwargs['name']
        level = kwargs['level']
        if (level <= 0) or (level > 4)  :
            error_case = True
            errormsg = "Unavailable 'level' value "+str(level)
            errordetails = "Available 'level' values : 1,2,3,4"
        if not error_case :
            try : 
                usr = User.objects.get(username=usr_name)
            except User.DoesNotExist as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
                errormsg = "User with "+usr_name+" as name does not exist"
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                ocnname_list = list()
                for usr_access in usr.access_list.all().filter(level=level) :
                    ocnname_list.append(usr_access.ocn.name)
                context = ocnname_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response


################################ Software #####################################


class SoftwareAccessUserNameListView(AccessViewMixin,
                                     generics.RetrieveAPIView):
    """Returns the Users having access to the Software

    Returns the list of the names of users (User) with an access (any level)
    to the software (Software) named 'name'
    """

    def get_queryset(self):
        return Software.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        sw_name = kwargs['name']
        try : 
            sw = Software.objects.get(name=sw_name)
        except Software.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "Software with "+sw_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                username_list = list()
                ocn = sw.ocn
                for ocn_access in ocn.access_list.all() :
                    username_list.append(ocn_access.usr.username)
                context = username_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class SoftwareAccessLevelUserNameListView(AccessViewMixin,
                                          generics.RetrieveAPIView):
    """Returns the Users having 'level' access to the Software

    Returns the list of the names of users (User) with an access ('level'
    level) to the software (Software) named 'name'
    """

    def get_queryset(self):
        return Software.objects.all()
                                       
    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        sw_name = kwargs['name']
        level = kwargs['level']
        if (level <= 0) or (level > 4)  :
            error_case = True
            errormsg = "Unavailable 'level' value "+str(level)
            errordetails = "Available 'level' values : 1,2,3,4"
        if not error_case :
            try : 
                sw = Software.objects.get(name=sw_name)
            except Software.DoesNotExist as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
                errormsg = "Software with "+sw_name+" as name does not exist"
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                username_list = list()
                ocn = sw.ocn
                for ocn_access in ocn.access_list.all().filter(level=level) :
                    username_list.append(ocn_access.usr.username)
                context = username_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class UserAccessSoftwareNameListView(AccessViewMixin,
                                     generics.RetrieveAPIView):
    """Returns the Softwares to which the User has access

    Returns the list of the names of softwares (Software) for which the
    user (User) named 'name' has an access (any level)
    """

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        usr_name = kwargs['name']
        try : 
            usr = User.objects.get(username=usr_name)
        except User.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "User with "+usr_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                swname_list = list()
                for usr_access in usr.access_list.all() :
                    for sw in usr_access.ocn.sw_list.all() :
                        swname_list.append(sw.name)
                context = swname_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

class UserAccessLevelSoftwareNameListView(AccessViewMixin,
                                            generics.RetrieveAPIView):
    """Returns the Softwares to which the User has 'level' access

    Returns the list of the names of softwares (Software) for which the
    user (User) named 'name' has an access ('level' level)
    """

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, **kwargs):

        #to_log(request)

        (error_case, errortype, errordetails, errormsg) = \
                                            (False, None, None, "") # default
        usr_name = kwargs['name']
        level = kwargs['level']
        if (level <= 0) or (level > 4)  :
            error_case = True
            errormsg = "Unavailable 'level' value "+str(level)
            errordetails = "Available 'level' values : 1,2,3,4"
        if not error_case :
            try : 
                usr = User.objects.get(username=usr_name)
            except User.DoesNotExist as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
                errormsg = "User with "+usr_name+" as name does not exist"
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if not error_case :
            try :
                swname_list = list()
                for usr_access in usr.access_list.all().filter(level=level) :
                    for sw in usr_access.ocn.sw_list.all() :
                        swname_list.append(sw.name)
                context = swname_list
                response = Response(context)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)
        return response

