"""acs.urls
  
Urls of the acs application

"""

from django.urls import path

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from acs.views import views

urlpatterns = [

    # JWT Authentication
    path('jwt/obtain/', obtain_jwt_token, name="token-auth"),
   #path('jwt/refresh/', refresh_jwt_token),
    path('jwt/verify/', verify_jwt_token, name="token-verify"),
    path('login', obtain_jwt_token),

    ######### User and OContainer

    # list of the names of users (User) with an access (any level)
    # to the container (OContainer) named 'name'
    path('container/<str:name>/access/user/name/list/',
         views.OContainerAccessUserNameListView.as_view(),
         name='acs-container-user-name-list' ),

    # list of the names of users (User) with an access ('level' level)
    # to the container (OContainer) named 'name'
    path('container/<str:name>/access/<int:level>/user/name/list/',
         views.OContainerAccessLevelUserNameListView.as_view(),
         name='acs-level-container-user-name-list' ),

    # list of the names of containers (OContainer) for which the user (User) 
    # named 'name' has an access (any level)
    path('user/<str:name>/access/container/name/list/',
         views.UserAccessOContainerNameListView.as_view(),
         name='acs-user-container-name-list' ),

    # list of the names of containers (OContainer) for which the user (User) 
    # named 'name' has an access ('level' level)
    path('user/<str:name>/access/<int:level>/container/name/list/',
         views.UserAccessLevelOContainerNameListView.as_view(),
         name='acs-level-user-container-name-list' ),

    ######### User and Software

    # list of the names of users (User) with an access (any level)
    # to the software (Software) named 'name'
    path('software/<str:name>/access/user/name/list/',
         views.SoftwareAccessUserNameListView.as_view(),
         name='acs-software-user-name-list' ),

    # list of the names of users (User) with an access ('level' level)
    # to the software (Software) named 'name'
    path('software/<str:name>/access/<int:level>/user/name/list/',
         views.SoftwareAccessLevelUserNameListView.as_view(),
         name='acs-level-software-user-name-list' ),

    # list of the names of softwares (Software) for which the user (User) 
    # named 'name' has an access (any level)
    path('user/<str:name>/access/software/name/list/',
         views.UserAccessSoftwareNameListView.as_view(),
         name='acs-user-software-name-list' ),

    # list of the names of softwares (Software) for which the user (User) 
    # named 'name' has an access ('level' level)
    path('user/<str:name>/access/<int:level>/software/name/list/',
         views.UserAccessLevelSoftwareNameListView.as_view(),
         name='acs-level-user-software-name-list' ),
]

def url_desc(name) :
    desc_text = ""

    # container ---

    if name == 'acs-container-user-name-list' :
        desc_text = "name list of users with an access (any level) to the container named 'name'"
         
    elif name == 'acs-level-container-user-name-list' :
        desc_text = "name list of users with an access ('level' level) to the container named 'name'"
         
    elif name == 'acs-user-container-name-list' :
        desc_text = "name list of containers for which the user named 'name' has an access (any level)"
         
    elif name == 'acs-user-container-name-list' :
        desc_text = "name list of containers for which the user named 'name' has an access ('level' level)"

    # software ---

    elif name == 'acs-software-user-name-list' :
        desc_text = "name list of users with an access (any level) to the software named 'name'"
         
    elif name == 'acs-level-software-user-name-list' :
        desc_text = "name list of users with an access ('level' level) to the software named 'name'"
         
    elif name == 'acs-user-software-name-list' :
        desc_text = "name list of softwares for which the user named 'name' has an access (any level)"
         
    elif name == 'acs-user-software-name-list' :
        desc_text = "name list of softwares for which the user named 'name' has an access ('level' level)"

    return desc_text

