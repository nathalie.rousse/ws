"""acs.admin"""

from django.contrib import admin

from acs.models import Access

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
admin.site.unregister(Group)
#admin.site.unregister(User)

from sw.models import OContainer

from acs.forms import AccessForm

acs_general_description = ""
ACS_GENERAL_DESCRIPTION = "\
      <p>Access is used to limit accesses.</p>\
      <p>Access makes a link between (only) one User and (only) one \
         OContainer with (only) one kind of level.</p>\
      <p>Level values : </p>\
      <p>- 1 <i>(visitor)</i> :\
           has minimum rights, no access to some parameters.</p>\
      <p>- 2 <i>(user)</i> : has access \
           to most of parameters but maybe with limited values domain.</p>\
      <p>- 3 <i>(superuser)</i> : has access \
           to most of parameters generally without limitations.</p>\
      <p>- 4 <i>(admin)</i> : has all rights.</p>\
      "

acs_identification_description = ""

class AccessAdmin(admin.ModelAdmin):
    form = AccessForm
    list_display = ('usr', 'ocn', 'level',) # 'id',
    list_filter = ['usr', 'ocn', 'level',]
    search_fields = ['usr', 'ocn',]

    fieldsets = (

        (None, {
            #'description': acs_general_description,
            'description': '<div>%s</div>' % ACS_GENERAL_DESCRIPTION,
            'fields': (),
        }),


        ('Identification', {
            'description': acs_identification_description,
            'fields': ('usr', 'ocn', 'level', 'text',),
        }),

    )

admin.site.register(Access, AccessAdmin)

