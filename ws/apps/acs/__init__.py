"""acs
  
*This package is part of ws web services*

:copyright: Copyright (C) 2020-2024 INRAE http://www.inrae.fr
:license: GPLv3, see LICENSE file for more details
:authors: see AUTHORS file

Access control application
 
The acs application is dedicated to access control management, based on JWT
(JSON Web Token).

"""

