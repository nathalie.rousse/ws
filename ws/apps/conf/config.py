"""conf.config

Contains the configuration : paths, constants...

"""

import os
import datetime

from cmn.utils.urls import get_host_name

def get_localdocs_root_url(request) :
    return ('http://' + get_host_name(request))

OPT_LOCATION = True
"""OPT_LOCATION value False or True \n
True if ws is located under '/opt', and else relative location
"""

if OPT_LOCATION is True :
    PACKAGE_HOME = "/opt/ws/ws"
    """ws package home path"""
else :
    PACKAGE_HOME = os.path.normpath(os.path.join(base_dir, "..", "..", ".."))
    """ws package home path"""

PROJECT_HOME = os.path.normpath(os.path.join(PACKAGE_HOME, ".."))
"""ws project home path"""

fab_path = os.path.normpath(os.path.join(PROJECT_HOME, "fab"))
"""factory path"""

#DELIV_HOME = os.path.normpath(os.path.join(PROJECT_HOME, "deliv"))
#"""DELIV_HOME deliveries home (exported)""" # __todo__ to delete

#SIMG_HOME = os.path.normpath(os.path.join(PROJECT_HOME, "simg"))
#"""SIMG_HOME singularity image files home (exported)""" # __todo__ to delete
                                                # (see WSM_LOCAL_SIMG_HOME...)

#RUN_HOME = os.path.join(fab_path, "run")
#"""RUN_HOME run home (exported)""" # __todo__ to delete
                                   # (see WSM_LOCAL_REQUESTS_HOME...)


# Files names
WS_STDOUT_FILENAME = "__WS__stdout.txt"
WS_STDERR_FILENAME = "__WS__stderr.txt"
WS_CMD_FILENAME =    "__WS__cmd.txt"
WS_REPORT_FILENAME = "__WS__report.txt"

IN_PRODUCTION = False # True or False
"""IN_PRODUCTION value False or True \n
In case of IN_PRODUCTION, no console destination for logging information ...
"""

# Closed media files (used for downloadable files)

CLOSEDMEDIA_DESTINATION_HOME = os.path.join(fab_path, 'closedmedia')
"""Closed media files home directory path, where access is controlled
(where a subdirectory for each project)
"""

# Static files

STATIC_DESTINATION_HOME =  os.path.join(fab_path, 'static')
"""Static files home directory path (where a subdirectory for each project)"""

"""Static files of XXX app"""
STATIC_CMN = os.path.join(PACKAGE_HOME, 'apps', 'cmn', 'static')
#STATIC_XXX = os.path.join(PACKAGE_HOME, 'apps', 'xxx', 'static')

INSTALL_HOME_FAB = os.path.join(fab_path, "install")
"""install home path, with produced/generated content"""

# Resources (folders, users...)
#
# Remote resources using suppose mounts between :
# - {rsrc}_REMOTE_REQUESTS_HOME (at {rsrc}_REMOTE_HOST) and
#   {rsrc}_LOCAL_REQUESTS_HOME
# - {rsrc}_REMOTE_SIMG_HOME (at {rsrc}_REMOTE_HOST) and
#   {rsrc}_LOCAL_SIMG_HOME
# See install/*/install.txt

RSRC_HOME = os.path.normpath(os.path.join(PROJECT_HOME, "..", "ws_rsrc"))
"""resources home path, where a subfolder by resource"""

JOB_ID_FILE_NAME = "JOB_ID"
"""cluster job id file name"""

# ws machine resource

WSM_HOME = os.path.join(RSRC_HOME, "wsm")
"""ws machine home path"""

WSM_LOCAL_SIMG_HOME = os.path.join(WSM_HOME, "simg")
WSM_LOCAL_REQUESTS_HOME = os.path.join(WSM_HOME, "requests")

# muse cluster remote resource

MUSE_HOME = os.path.join(RSRC_HOME, "muse")
"""muse cluster home path"""

MUSE_LOCAL_SIMG_HOME = os.path.join(MUSE_HOME, "simg")
MUSE_LOCAL_REQUESTS_HOME = os.path.join(MUSE_HOME, "requests")

MUSE_REMOTE_HOST = "roussen@muse-login.hpc-lr.univ-montp2.fr"
                 # Note : if decomposition required :
                 # MUSE_REMOTE_MACHINE = "muse-login.hpc-lr.univ-montp2.fr"
                 # MUSE_REMOTE_USER = "roussen"
                 # MUSE_REMOTE_HOST = MUSE_REMOTE_USER+"@"+MUSE_REMOTE_MACHINE
MUSE_REMOTE_REQUESTS_HOME = "/home/roussen/scratch/ws/requests"
MUSE_REMOTE_SIMG_HOME = "/home/roussen/ws/simg"

# Closed media files structure (from CLOSEDMEDIA_ROOT)
# (used for downloadable files)

DOWNLOADS_HOME = "load" # from CLOSEDMEDIA_ROOT
"""closed media structure for downloadable files. \n
DOWNLOADS_HOME is a 'root' dedicated to load (the application managing
downloadable files), divided by subdirectories. \n
DOWNLOADS_HOME path is relative to CLOSEDMEDIA_ROOT.
"""

DOWNLOADS_HOME_DEFAULT = os.path.join(DOWNLOADS_HOME, "default")
"""closed media structure for downloadable files by default. \n
DOWNLOADS_HOME_DEFAULT is relative to CLOSEDMEDIA_ROOT (like DOWNLOADS_HOME).
"""

DOWNLOADS_HOME_SW = os.path.join(DOWNLOADS_HOME, "sw")
"""closed media structure for downloadable files about sw application. \n
DOWNLOADS_HOME_SW is relative to CLOSEDMEDIA_ROOT (like DOWNLOADS_HOME).
"""

# log

LOG_ACTIVE = True # False
LOG_FILE = os.path.join(fab_path, 'log', 'ws.log' )
"""File destination of logging information \n
Value None for no file destination for logging information
"""

# databases (only one)

DB_NAME_DEFAULT = 'default'
"""Database name of default database (admin...)"""

DB_PATH_DEFAULT =  os.path.join(PROJECT_HOME, 'db', 'ws.sqlite3')
"""Database file path of DB_NAME_DEFAULT database"""

# templates

## templates of each app
TEMPLATE_SW = os.path.join(PACKAGE_HOME, 'apps', 'sw', 'templates')
"""templates of sw app"""
TEMPLATE_LOAD = os.path.join(PACKAGE_HOME, 'apps', 'load', 'templates')
"""templates of load app"""
TEMPLATE_CMN = os.path.join(PACKAGE_HOME, 'apps', 'cmn', 'templates')
"""templates of cmn app"""
#TEMPLATE_XXX = os.path.join(PACKAGE_HOME, 'apps', 'xxx', 'templates')


# some software behavior options

CLUSTERACT_LIFETIME = 10
"""Age criterion to delete a folder about a ClusterAct (into run space into \
fab) ; see delete_if_old method, delete_too_old_clusteract (admin) \n
unit : number of days
"""

DOWNLOAD_LIFETIME = 21
"""Age criterion to delete a folder into download space into factory (see
delete_if_old method, and also delete_too_old_downloadvpzdocument admin
command) \n
unit : number of days
"""

TOKEN_LIFETIME = datetime.timedelta(seconds=36000) # 10h
"""Duration before a token expiration. TOKEN_LIFETIME is an instance of Python's datetime.timedelta. Examples : datetime.timedelta(seconds=300), datetime.timedelta(days=5).
"""

