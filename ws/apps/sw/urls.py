"""sw.urls
  
Urls of the sw application

"""

from django.urls import path

from sw.views import objects as sw_objects
from sw.views import views as sw_views

urlpatterns = [

    # list and detail

    path('software/list/',
         sw_objects.SwList.as_view(), name='sw-software-list' ),

    path('software/detail/<str:name>/',
         sw_objects.SwDetail.as_view(), name='sw-software-detail' ),

    path('container/list/',
         sw_objects.OcnList.as_view(), name='sw-container-list' ),

    # run

    path('software/run/<str:name>/',
         sw_views.SwRun.as_view(), name='sw-software-run' ),

    path('software/muse/run/<str:name>/',
         sw_views.SwMuseRun.as_view(),
         name='sw-software-muse-run-post' ),
    path('software/muse/run/<str:name>/<str:key>/',
         sw_views.SwMuseRun.as_view(),
         name='sw-software-muse-run-get' ),
    path('software/muse/state/<str:name>/<str:key>/',
         sw_views.SwMuseState.as_view(),
         name='sw-software-muse-state-get' ),
    path('software/muse/content/<str:name>/<str:key>/',
         sw_views.SwMuseContent.as_view(),
         name='sw-software-muse-content-get' ),

]

def url_desc(name) :
    desc_text = ""
    if name == 'sw-software-list' :
        desc_text = "software list"
    elif name == 'sw-software-detail' :
        desc_text = "a software detail"
    elif name == 'sw-software-run' :
        desc_text = "software local launch"
    elif name == 'sw-software-muse-run-post' :
        desc_text = "software launch on muse cluster"
    elif name == 'sw-software-muse-run-get' :
        desc_text = "get results of software launch on muse cluster"
    elif name == 'sw-software-muse-state-get' :
        desc_text = "get cluster job state of software launch on muse cluster"
    elif name == 'sw-software-muse-content-get' :
        desc_text = "get cluster job folder of software launch on muse cluster"
    return desc_text

