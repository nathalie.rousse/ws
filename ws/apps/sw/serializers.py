"""sw.serializers

Serializers for sw application

"""

from rest_framework import serializers

class CmdSerializer(serializers.Serializer) :
    """Option 'cmd' """

    cmd = serializers.CharField(required=False)
    def validate(self, attrs):
        if 'cmd' not in attrs.keys() :
            attrs['cmd']=None
        return attrs

class CmdOptionSerializer(serializers.Serializer) :
    """Option 'cmd_option' """

    cmd_option = serializers.CharField(required=False)
    def validate(self, attrs):
        if 'cmd_option' not in attrs.keys() :
            attrs['cmd_option']=None
        return attrs

class ReturnedTypeSerializer(serializers.Serializer) :
    """Option 'returned_type' """

    returned_type = serializers.CharField(required=False)
    def validate(self, attrs):
        if 'returned_type' not in attrs.keys() :
            attrs['returned_type']=None
        return attrs

class TodownloadSerializer(serializers.Serializer) :
    """Option 'todownload' """

    todownload = serializers.CharField(required=False)
    def validate(self, attrs):
        if 'todownload' not in attrs.keys() :
            attrs['todownload']=None
        return attrs

