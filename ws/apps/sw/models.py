"""sw.models
  
Models of the sw application

"""

import uuid
import datetime

from django.db import models
from django.db.models.signals import pre_delete

from cmn.models_mixins.info import InfoMixin
from cmn.models_mixins.priv import PrivMixin

from conf.config import CLUSTERACT_LIFETIME

from conf.config import get_localdocs_root_url
from cmn.utils.urls import get_absolute_url
from cmn.utils.folder_file import delete_path_if_present

MAX_LENGTH_OCNIMAGE  = 100
MAX_LENGTH_OCNDESC  = 1000000
ht_ocn_name = "Name identifying the container into the database (unique)"
ht_ocn_type = "Type of container ('docker' or 'singularity')"
ht_ocn_image = "Image name in docker case, name of image file in singularity case" 
ht_ocn_desc = "Container text description"

MAX_LENGTH_SWINFO  = 1000000
MAX_LENGTH_SWPRIV  = 1000000
ht_sw_name = "Name identifying the software into the database (unique)"
ht_sw_ocn = "software container into the database"
ht_sw_info = "software data information"
ht_sw_priv = "software private information"

MAX_LENGTH_ACTINFO  = 1000000
MAX_LENGTH_ACTPRIV  = 1000000
ht_clu_key = "Key identifying the cluster activity into the database (unique)"
ht_clu_name = "Name of the involved software (not unique)"
ht_clu_info = "cluster activity data information"
ht_clu_priv = "cluster activity private information"

######################## OContainer for Softwares ##############################

class OContainer(models.Model):
    """Official/Original container

    Attributes :

    - name : name given to the OContainer for ws (unique)

    - type : type of container. Available values : 'docker', 'singularity'

    - image : image name in docker case, name of image file in singularity case

    - desc : container text description

    - access_list : list of Access

    """

    name = models.CharField(max_length=200, unique=True, blank=False,
                            help_text=ht_ocn_name)

    type = models.CharField(max_length=12, blank=True,
                            default="", help_text=ht_ocn_type)

    image = models.CharField(max_length=MAX_LENGTH_OCNIMAGE, blank=True,
                            default="", help_text=ht_ocn_image)

    desc = models.CharField(max_length=MAX_LENGTH_OCNDESC, blank=True,
                            default="", help_text=ht_ocn_desc)

    def __str__(self):
        return self.name+" ("+self.image+")"

    def get_type(self):
        ocn_type = None # default
        v = self.type
        if v in ("docker", "singularity") :
            ocn_type = v
        return ocn_type

    def get_image(self):
        return self.image

    def get_name(self):
        return self.name

    def get_desc(self):
        return self.desc

    def control(self) :
        """Controls and returns some container information (type and image) """

        ocn_type = self.get_type()
        ocn_image = self.get_image()

        if ocn_type is None :
            raise Exception("unavailable container type")

        if ocn_type == "docker" :

            if ocn_image is None : 
                raise Exception("unavailable image of docker container")
            #elif does not exist : __todo__
            #    raise Exception("unexisting image of docker container")

        elif ocn_type == "singularity" :

            if ocn_image is None :
                msg = "unavailable image file of singularity container"
                raise Exception(msg)
            #elif does not exist : __todo__
            #    msg = "unexisting image file of singularity container"
            #    raise Exception(msg)

        else : # impossible
            raise Exception("unavailable container type")

        return (ocn_type, ocn_image)

    class Meta:
        verbose_name = "container"
        verbose_name_plural = "containers"

################################ Software #####################################

class Software(PrivMixin, InfoMixin, models.Model):
    """Software

    Attributes :

    - name : name given to the software for ws (unique)

    - info : data information (JSON value as str) for

             dict {
               - "sw" dict {
                 - "shortname" : short name
                 - "desc" : text description
                 - "urls" : dict {title:value}
                 - "keywords" : list [value,..]
                 }
             }

    - ocn : relative OContainer ((many Software to one OContainer relationship)
            (note : OContainer is with type "docker" or "singularity")

    - priv : private information (JSON value as str) for

             dict {
               - "localdocs" : dict {title:value} 
                               to modify to be added to urls
             }
 
    Plus :

    - localdocs_root_url : used to define urls from localdocs

    """

    name = models.CharField(max_length=200, unique=True, blank=False,
                            help_text=ht_sw_name)

    # ws_072021 delete OContainer => Software deleted
    ocn = models.ForeignKey(OContainer, on_delete=models.CASCADE,
                            related_name="sw_list",
                            verbose_name="related OContainer", blank=True,
                            help_text=ht_sw_ocn)

    info = models.CharField(max_length=MAX_LENGTH_SWINFO, blank=True,
                            default="", help_text=ht_sw_info)

    priv = models.CharField(max_length=MAX_LENGTH_SWPRIV, blank=True,
                            default="", help_text=ht_sw_priv)

    def __str__(self):
        return self.name+" ("+self.ocn.name+")"

    def get_name(self):
        return self.name

    def get_ocn_name(self):
        return self.ocn.name

    def extract_from_info(self) :
        """extract_from_info with Software particularities

        Inserts localdocs information into urls (as absolute urls)
        """

        I = super(Software, self).extract_from_info()

        # add localdocs into urls
        localdocs = self.get_priv_localdocs()
        if localdocs is not None :
            if "sw" in I.keys() :
                if "urls" not in I["sw"].keys() :
                    I["sw"]["urls"] = dict()
                if type(I["sw"]["urls"]) is dict :
                    for title in localdocs.keys() :
                        I["sw"]["urls"][title] = get_absolute_url(
                                             root_url=self.localdocs_root_url,
                                             relative_url=localdocs[title])
        return I

    def complete_ini(self, request) :
        self.localdocs_root_url = get_localdocs_root_url(request)

    def get_info_cn(self) :
        """Controls and returns some container information (type and image) """

        ocn = self.ocn
        (ocn_type, ocn_image) = ocn.control()
        return (ocn_type, ocn_image)

    @classmethod
    def get_localdocs(cls, P) :
        localdocs = None # default
        if 'localdocs' in P.keys() :
            d = P['localdocs']
            if type(d) is dict :
                localdocs = d
        return localdocs

    def get_priv_localdocs(self) :
        P = self.extract_from_priv()
        if P is None :
            return None
        elif type(P) is dict : 
            return self.get_localdocs(P)
        else :
            return None

    class Meta:
        verbose_name = "software"
        verbose_name_plural = "softwares"

######################## Activities on Softwares ##############################

class LocalAct(object):
    """Local activity

    There for homogeneity between cluster case et local (wsm) case
    """

    @classmethod
    def begin_report(cls, name, cmd, cmd_option, ordered_filename_list,
                    returned_type, todownload):
        """To be written 'as' ClusterAct.begin_report
    
        report common to ClusterAct and LocalAct, excepted :
            - sbatch_list and module_list fields of "request" key
            - "rsrc" key and "process" key
    
        report remains to be completed :
        - report (local as cluster case) will also contain a "cr" key

        """

        report = { "name" : name,
                   "request": { "cmd" : cmd,
                                "cmd_option":cmd_option,
                                "ordered_filename_list":ordered_filename_list,
                                "returned_type":returned_type,
                                "todownload":todownload,
                   }
        }
        return report


class ClusterAct(PrivMixin, InfoMixin, models.Model):
    """Cluster activity

    Attributes :

    - key : key given to the cluact for ws (unique)
    - name : name of the involved software (not unique)
                                           (note : no Model Relation)
    - date : date

    - info : data information (JSON value as str) for

             dict {

                 - "request" dict : {
                    - "cmd" key
                    - "cmd_option" key
                    - "ordered_filename_list" key
                    - "returned_type" key
                    - "todownload" key
                    - "sbatch_list" key
                    - "module_list" key
                   }
                 - "rsrc" dict : {
                    - "identity" key
                   }
                 - "process" dict : {
                    - "jobname" key
                    - "job_id" key
                    - "latest_cr" key
                    - "latest_state" key
                   }
               }

    - priv : private information (JSON value as str) for

             dict {

                 - "request" dict : {
                   - "download_base_url" key
                   - "jwt" key
                 }
                 - "wk" dict : {
                    - "home_path" key (useful to folder deletion)
                    - "run_path" key
                   }
             }

    ClusterAct deletion (with its associated home_path) :
    - admin page -> action 'Delete selected ClusterAct if too old'
    Note : there could also be other conditions ("latest_cr", "latest_state"...)

    """

    date = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=200, blank=False, help_text=ht_clu_key)
    name = models.CharField(max_length=200, blank=False, help_text=ht_clu_name)
    info = models.CharField(max_length=MAX_LENGTH_ACTINFO, blank=True,
                            default="", help_text=ht_clu_info)
    priv = models.CharField(max_length=MAX_LENGTH_ACTPRIV, blank=True,
                            default="", help_text=ht_clu_priv)

    def is_old(self):
        """ClusterAct is old if date 'older' than CLUSTERACT_LIFETIME """

        return self.is_old_indays(days=CLUSTERACT_LIFETIME)

    def delete_if_old(self):
        """Deletes ClusterAct and its associated folder, if too old """

        if self.is_old() :
            self.delete()

    def ini_save(self, name) :
        """Initialisation of cluster activity """

        def define_key_value():
            return str(uuid.uuid4())

        key = define_key_value()
        self.key = key
        self.name = name
        self.save()

    def update_save_from(self, request=None, wk=None, rsrc=None, process=None) :
        """Updates info and priv and save them into the database """

        self.info_update_save_from(request=request, wk=wk, rsrc=rsrc,
                                   process=process)
        self.priv_update_save_from(request=request, wk=wk, rsrc=rsrc,
                                   process=process)

    def priv_update_save_from(self, request=None, wk=None, rsrc=None,
                              process=None) :
        """Updates priv and saves it into the database """

        P = self.extract_from_priv()
        if type(P) is not dict :
            P = dict()
        keys = P.keys()

        if type(request) is dict :
            if "request" not in keys:
                P["request"] = dict()
            priv_request = dict() # filtering request
            priv_keys = ("download_base_url", "jwt")
            for key in request.keys() :
                if key in priv_keys :
                    priv_request[key] = request[key]
            P["request"].update(priv_request)
        if type(wk) is dict :
            if "wk" not in keys:
                P["wk"] = dict()
            P["wk"].update(wk)

        self.priv_update_and_save_from(P)

    def info_update_save_from(self, request=None, wk=None, rsrc=None,
                              process=None) :
        """Updates info and saves it into the database """

        I = self.extract_from_info()
        if type(I) is not dict :
            I = dict()
        keys = I.keys()

        if type(request) is dict :
            if "request" not in keys:
                I["request"] = dict()
            info_request = dict() # filtering request
            info_keys = ("cmd", "cmd_option", "ordered_filename_list",
                         "returned_type", "todownload", "sbatch_list",
                         "module_list")
            for key in request.keys() :
                if key in info_keys :
                    info_request[key] = request[key]
            I["request"].update(info_request)
        if type(rsrc) is dict :
            if "rsrc" not in keys:
                I["rsrc"] = dict()
            I["rsrc"].update(rsrc)
        if type(process) is dict :
            if "process" not in keys:
                I["process"] = dict()
            I["process"].update(process)

        self.info_update_and_save_from(I)

    def define_jobname(self, name) :
        jobname = str(self.id) + "-ws-" + name
        return jobname

    def begin_report(self, I):
        """To be written 'as' LocalAct.begin_report"""
        report = I
        report["name"] = self.name
        return report

    @classmethod
    def get_returned_type_from(cls, I, P) :
        """Returns I["request"]["returned_type"] or None"""

        returned_type = None # default
        if 'request' in I.keys() :
            request = I['request']
            if 'returned_type' in request.keys() :
                returned_type = request['returned_type']
        return returned_type

    @classmethod
    def get_todownload_from(cls, I, P) :
        """Returns I["request"]["todownload"] or None"""

        todownload = None # default
        if 'request' in I.keys() :
            request = I['request']
            if 'todownload' in request.keys() :
                todownload = request['todownload']
        return todownload

    @classmethod
    def get_download_base_url_from(cls, I, P) :
        """Returns P["request"]["download_base_url"] or None """

        download_base_url = None # default
        if 'request' in P.keys() :
            request = P['request']
            if 'download_base_url' in request.keys() :
                download_base_url = request['download_base_url']
        return download_base_url

    @classmethod
    def get_wk_home_path_from(cls, I, P) :
        """Returns P["wk"]["home_path"] if existing and else None """

        home_path = None # default
        if 'wk' in P.keys() :
            wk = P['wk']
            if 'home_path' in wk.keys() :
                home_path = wk['home_path']
        return home_path

    def get_wk_home_path(self) :
        return self.get_wk_home_path_from(I=self.extract_from_info(),
                                          P=self.extract_from_priv())

    @classmethod
    def get_wk_run_path_from(cls, I, P) :
        """Returns P["wk"]["run_path"] if existing and else None """

        run_path = None # default
        if 'wk' in P.keys() :
            wk = P['wk']
            if 'run_path' in wk.keys() :
                run_path = wk['run_path']
        return run_path

    def get_wk_run_path(self) :
        return self.get_wk_run_path_from(I=self.extract_from_info(),
                                         P=self.extract_from_priv())

    @classmethod
    def get_process_job_id_from(cls, I, P) :
        """Returns existing I["process"]["job_id"] or None """

        job_id = None # default
        if 'process' in I.keys() :
            process = I['process']
            if 'job_id' in process.keys() :
                job_id = process['job_id']
        return job_id

    def get_process_job_id(self) :
        return self.get_process_job_id_from(I=self.extract_from_info(),
                                            P=self.extract_from_priv())

    @classmethod
    def get_process_latest_cr_from(cls, I, P) :
        """Returns existing I["process"]["latest_cr"] or None """

        latest_cr = None # default
        if 'process' in I.keys() :
            process = I['process']
            if 'latest_cr' in process.keys() :
                latest_cr = process['latest_cr']
        return latest_cr

    def get_process_latest_cr(self) :
        return self.get_process_latest_cr_from(I=self.extract_from_info(),
                                               P=self.extract_from_priv())

    @classmethod
    def get_process_latest_state_from(cls, I, P) :
        """Returns existing I["process"]["latest_state"] or None """

        latest_state = None # default
        if 'process' in I.keys() :
            process = I['process']
            if 'latest_state' in process.keys() :
                latest_state = process['latest_state']
        return latest_state

    def get_process_latest_state(self) :
        I = self.extract_from_info()
        return self.get_process_latest_state_from(I=self.extract_from_info(),
                                                  P=self.extract_from_priv())

    def update_cluster_process(self, rsrc) :
        """Reads cluster process state and updates process information"""

        I = self.extract_from_info()
        P = self.extract_from_priv()

        run_path = self.get_wk_run_path_from(I, P)
        if run_path is None :
            raise Exception("unavailable inputs workspace run_path")

        if "process" not in I.keys() :
            I["process"] = dict()
        process = I["process"]

        if "job_id" in process.keys() :
            job_id = process["job_id"]
        else :
            try :
                job_id = rsrc.try_get_job_id(local_run_path=run_path)
            except :
                raise
            if job_id is not None :
                process["job_id"] = job_id

        if job_id is not None :
            finish = False
            if "latest_cr" in process.keys() :
                latest_cr = process["latest_cr"]
                if (latest_cr == "ENDED_OK") or (latest_cr == "ENDED_NOK") :
                    finish = True
            if not finish : # latest_cr undefined or "IN_PROGRESS" 
                (cr, state) = rsrc.get_job_state(job_id=job_id)
                process["latest_cr"] = cr
                process["latest_state"] = state # maybe ""
        return process

    def clear_dir(self):
        """Deletes (if exists) the folder relative to clusteract"""

        delete_path_if_present(path=self.get_wk_home_path())

    def is_old_indays(self, days):
        """ClusterAct is old if date 'older' than days """

        d = self.date + datetime.timedelta(days=days)
        now = datetime.datetime.now()
        if d.toordinal() < now.toordinal() :
           return True
        else :
           return False

def clusteract_clear(sender, instance, *args, **kwargs):
    """Deletes ClusterAct associated folder"""

    instance.clear_dir()

# done before a ClusterAct deletion
pre_delete.connect(clusteract_clear, sender=ClusterAct)

