"""sw.admin"""

from django.contrib import admin
from sw.models import Software
from sw.models import OContainer
from sw.models import ClusterAct
from acs.models import Access
from sw.forms import SoftwareForm
from sw.forms import OContainerForm
from sw.forms import ClusterActForm
from softwares.doc.default_doc.build_software_default_doc import build_html_home_page
from softwares.install.build_script_building_singularity_from_docker import generate_script_file

sw_general_description = ""
SW_GENERAL_DESCRIPTION = "\
      <p>Fields : </p>\
      <p>- A software is identified by its <b>name</b> field (unique). </p>\
      <p>- A software is delivered as a container, identified there by its name <b>Ocn name</b>. </p>\
      <p>- <b>info</b> field : information dedicated to users. </p>\
      <p>- <b>priv</b> field : some internal information. </p>\
      <p>To describe a software : </p>\
      <p>- a text <b>desc</b>, some external <b>urls</b> and some <b>keywords</b> can be given. </p>\
      <p>- Some documentation (see <b>localdocs</b>) can be delivered in order to be online. </p>\
      "

sw_container_description = "Note : The container must have previously been recorded into the database, in order to be chosen here !"
sw_identification_description = ""

SW_INFORMATION_DESCRIPTION = "\
     <p>Into <b>sw</b> software dictionary :</p>\
     <p>-- <b>desc</b> : text description\" </p>\
     <p>-- <b>shortname</b> : short name\" </p>\
     <p>-- <b>urls</b> : dictionary of <b>title</b>:<b>url value</b> </p>\
     <p>-- <b>keywords</b> : list of key words </p>\
     <p>Short help to fill : </p>\
     <p>{ </p>\
     <p> \"<b>sw</b>\":{ \
           \"<b>desc</b>\":\"...\", \
           \"<b>shortname</b>\":\"...\", \
           \"<b>urls</b>\":{\"...\":\"...\",...}, \
           \"<b>keywords</b>\":[\"...\",...] \
          }</p>\
     <p>}</p>\
     "

SW_PRIVATE_DESCRIPTION = "\
    <p> This information remains internal as is. </p>\
    <p><b>localdocs</b> will be \"added\" by ws to <b>urls</b>. </p>\
    <p> Short help to fill : { </p>\
    <p>  \"<b>localdocs</b>\":{\"default documentation\": \"softwares/software name/index.html\", \
                        \"title\":\"relative url value\",...} </p>\
    <p> } </p>\
    "

def generate_default_doc(modeladmin, request, queryset):
    for sw in queryset:
        build_html_home_page(software_name=sw.name)

generate_default_doc.short_description = \
                                       "Generate default doc (html home page)"

class SoftwareAdmin(admin.ModelAdmin):
    form = SoftwareForm
    list_display = ('name', 'ocn')
    actions = [generate_default_doc]
    list_filter = ['name', 'ocn']
    search_fields = ['name', 'ocn']

    fieldsets = (

        (None, {
            'description': '<div>%s</div>' % SW_GENERAL_DESCRIPTION,
            'fields': (),
        }),


        ('Software identification', {
            'description': sw_identification_description, 'fields': ('name',),
        }),

        ('Container', {
            'description': sw_container_description, 'fields': ('ocn',),
        }),

        ('Information', {
            'description': '<div>%s</div>' % SW_INFORMATION_DESCRIPTION,
            'fields': ('info',),
        }),

        ('Private', {
            'description': '<div>%s</div>' % SW_PRIVATE_DESCRIPTION,
            'fields': ('priv',),
        }),

    )

admin.site.register(Software, SoftwareAdmin)

def generate_singularity_from_docker_script_file(modeladmin, request, queryset):
    generate_script_file()

generate_singularity_from_docker_script_file.short_description = \
   "Generate singularity from docker script file (select at least 1 OContainer)"

container_general_description = ""
OCN_GENERAL_DESCRIPTION = "\
     <p>A container recorded into the database is in its <b>original form</b> (docker or singularity), ie the way how it has been delivered to ws. </p>\
     <p>In docker delivery case, the docker container will have to be converted into singularity before to be able to be used by ws, because ws manipulates only singularity containers. </p>\
     <p><u>Docker container conversion</u> :</p>\
     <p>- There is for each resource, a <b>simg folder</b> dedicated to singularity image files. </p>\
     <p>- A singularity image file will be produced from the docker image : hidden file \"<b>.name.simg</b>\" under simg folder. </p>\
     <p>- How to convert (from docker to singularity) : follow /opt/ws/<b>softwares/install/install.txt</b> (<i>build_script_building_singularity_from_docker.py, build_singularity_from_docker.sh...</i>). Also using \"<b>Generate singularity from docker script file</b>\" in Container Admin main page.</p>\
     <br> \
     "

ocn_name_description = "Container recorded into the database is the one as delivered, in its original form, ie as docker or singularity."
ocn_type_description = ""

OCN_IMAGE_DESCRIPTION = "\
     <p>In 'singularity' type case, the image file must be located under <b>simg folder</b>. </p>\
     <p>In 'docker' type case, the image name format is REPOSITORY:TAG ; see command : <i><b>docker image ls</b></i> . </p>\
     "

ocn_desc_description = ""

class OContainerInline(admin.TabularInline):
    model = OContainer

class AccessInline(admin.TabularInline):
    model = Access

class OContainerAdmin(admin.ModelAdmin):
    form = OContainerForm
    list_display = ('name', 'type', 'image',)
    actions = [generate_singularity_from_docker_script_file]
    inlines = [AccessInline]
    list_filter = ['type', 'image']
    search_fields = ['image']
    fieldsets = (

        (None, {
            'description': '<div>%s</div>' % OCN_GENERAL_DESCRIPTION,
            'fields': (),
        }),

        ('Name', {
            'description': ocn_name_description, 'fields': ('name',),
        }),

        ('Type', {
            'description': ocn_type_description, 'fields': ('type',),
        }),

        ('Image', {
            'description': '<div>%s</div>' % OCN_IMAGE_DESCRIPTION,
            'fields': ('image',),
        }),


        ('Description', {
            'description': ocn_desc_description, 'fields': ('desc',),
        }),
    )

admin.site.register(OContainer, OContainerAdmin)


clusteract_general_description = \
                      "ClusterAct associated with a cluster run of a Software"

def delete_too_old_clusteract(modeladmin, request, queryset):
    for clusteract in queryset :
        clusteract.delete_if_old()

delete_too_old_clusteract.short_description = \
                                       "Delete selected clusteract if too old"

class ClusterActAdmin(admin.ModelAdmin):
    form = ClusterActForm
    list_display = ('name', 'info', 'priv',)
    actions = [delete_too_old_clusteract]
    list_filter = ['name']
    search_fields = ['name']
    fieldsets = (
        (None, {
            'description': clusteract_general_description,
            'fields': (),
        }),

        ('Identification', {
            'fields': ('name', 'key',), # 'date',),
        }),

        ('Information', {
            'fields': ('info',),
        }),

        ('Private', {
            'fields': ('priv',),
        }),

    )

admin.site.register(ClusterAct, ClusterActAdmin)

