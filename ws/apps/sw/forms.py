"""sw.forms"""

from django import forms

from sw.models import Software
from sw.models import OContainer
from sw.models import ClusterAct

from sw.models import ht_ocn_type

class SoftwareForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SoftwareForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n == "info" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':100})
            elif n == "priv" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':50})

    class Meta:
        model = Software
        fields = '__all__'

class OContainerForm(forms.ModelForm):

    type = forms.ChoiceField( choices=[('docker','docker'),
                                       ('singularity','singularity')],
                              required=True, help_text=ht_ocn_type)

    def __init__(self, *args, **kwargs):
        super(OContainerForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n == "image" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':30})
            elif n == "desc" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':50})

    class Meta:
        model = OContainer
        fields = '__all__'

class ClusterActForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ClusterActForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n == "info" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':100})
            elif n == "priv" :
                f.widget=forms.Textarea(attrs={'rows':2, 'cols':50})

    class Meta:
        model = ClusterAct
        fields = '__all__'

