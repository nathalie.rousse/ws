"""sw.views.objects """

from rest_framework.response import Response
from rest_framework import generics
from rest_framework import serializers

from sw.models import Software
from sw.models import OContainer

from cmn.views_mixins import DataViewMixin
from cmn.views_mixins import FormatViewMixin
from cmn.views_mixins import FormatContentNegotiation

from cmn.utils.errors import complete_error_message
from cmn.utils.errors import get_exception_info

headsubtitle = "(sw)"

class SwList(FormatViewMixin, DataViewMixin, generics.ListAPIView):
    """List all the softwares (without information about their container) """

    model = Software

    content_negotiation_class = FormatContentNegotiation

    def get_queryset(self):
        return Software.objects.all()

    def get(self, request, **kwargs):
        error_case = False # default
        (errortype, errordetails, errormsg) = (None, None, "") # default
        try :
            context = list()
            for sw in self.get_queryset() :
                sw.complete_ini(request)
                c = {"name":sw.name, "info":sw.extract_from_info()}
                context.append(c)
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
        return Response(context)

class SwDetail(FormatViewMixin, DataViewMixin, generics.RetrieveAPIView):
    """One software detail (including information about it container) """

    model = Software

    content_negotiation_class = FormatContentNegotiation

    def get_queryset(self):
        return Software.objects.all()

    def get(self, request, **kwargs):

        error_case = False # default
        (errortype, errordetails, errormsg) = (None, None, "") # default
        sw_name = kwargs['name']
        try :
            sw = Software.objects.get(name=sw_name)
            sw.complete_ini(request)
            ocn = sw.ocn
            context = dict()
            context['name'] = sw.name
            context['info'] = sw.extract_from_info()
            context["cont"] = {"name" : ocn.name,
                               "type" : ocn.type,
                               "image" : ocn.image,
                               "desc" : ocn.desc }

        except Software.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "Software with "+sw_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
        return Response(context)


class OcnList(FormatViewMixin, DataViewMixin, generics.ListAPIView):
    """List all the containers"""

    model = OContainer

    content_negotiation_class = FormatContentNegotiation

    def get_queryset(self):
        return OContainer.objects.all()

    def get(self, request, **kwargs):
        error_case = False # default
        (errortype, errordetails, errormsg) = (None, None, "") # default
        try :
            context = list()
            for ocn in self.get_queryset() :
                c = {"name" : ocn.name,
                     "type" : ocn.type,
                     "image" : ocn.image,
                     "desc" : ocn.desc }
                context.append(c)
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
        return Response(context)

