"""sw.views.views """

from rest_framework import generics
from rest_framework import serializers

from sw.models import Software
from sw.models_mixins.resource import MuseResource

from sw.views.views_run_mixins import SwRunViewMixin

from cmn.utils.log import to_log

headsubtitle = "(sw)"


class SwRun(SwRunViewMixin, generics.RetrieveAPIView):
    """Runs a software

    'GET software/run/{name}' : unavailable.
    'POST software/run/{name}' : runs the 'name' software and returns result, \
    according to parameters ;

    Local run (ws machine), always as singularity command (container type is \
    docker or singularity).

    Note : If container type is docker, then the run singularity image must \
    have previously been built from the docker image (as required by ws).

    Note : 'GET software/list' : to list all available softwares.
    """

    serializer_class = serializers.Serializer

    def get_queryset(self):
        return Software.objects.all()

    def get_object(self, name, request):
        sw = Software.objects.get(name=name)
        sw.complete_ini(request)
        return sw

    def get(self, request, **kwargs):
        """GET for view SwRun : Unavailable request returning an error """

        response = self.get_local_run(request=request, **kwargs)
        return response

    def post(self, request, **kwargs):
        """POST for view SwRun

        Runs 'name' software and returns result, according to parameters.

        Local run (ws machine), always as singularity command (container type \
        is docker or singularity).
        """

        response = self.post_local_run(request=request, **kwargs)
        return response

class SwMuseState(SwRunViewMixin, generics.RetrieveAPIView):
    """Job state text about/after request to run a software under Muse cluster

    'GET  software/muse/state/{name}/{key}' request, after
    'POST software/muse/run/{name}' request and its response containing {key}

    (for more about 'POST software/muse/run/{name}' : see SwMuseRun)

    'GET software/muse/state/{name}/{key}' :

       Returns cluster job information as text (IN_PROGRESS, ENDED_OK,
       ENDED_NOK...).

    When cr is "OK" into "report" of the response, then the run results can be
    asked (see SwMuseRun).

    """

    serializer_class = serializers.Serializer

    def get_queryset(self):
        return Software.objects.all()

    def get_object(self, name, request):
        sw = Software.objects.get(name=name)
        sw.complete_ini(request)
        return sw

    def get(self, request, **kwargs):
        """GET for view SwMuseState """

        rsrc = MuseResource()
        response = self.get_remote_state(request=request, rsrc=rsrc, **kwargs)
        return response
        
    def post(self, request, **kwargs):
        """POST for view SwMuseState """

        to_log(request)

        response = self.unavailable_request(
                                     error_message="Unavailable POST request",
                                     more_message="Use GET request")
        return response

class SwMuseContent(SwRunViewMixin, generics.RetrieveAPIView):
    """Job content file about/after request to run a software under Muse cluster

    'GET  software/muse/content/{name}/{key}' request, after
    'POST software/muse/run/{name}' request and its response containing {key}

    (for more about 'POST software/muse/run/{name}' : see SwMuseRun)

    'GET software/muse/content/{name}/{key}' :

       Returns cluster job information as file : the whole run folder (.zip).

    """

    serializer_class = serializers.Serializer

    def get_queryset(self):
        return Software.objects.all()

    def get_object(self, name, request):
        sw = Software.objects.get(name=name)
        sw.complete_ini(request)
        return sw

    def get(self, request, **kwargs):
        """GET for view SwMuseContent """

        rsrc = MuseResource()
        response = self.get_remote_content(request=request,
                                           rsrc=rsrc, **kwargs)
        return response
        
    def post(self, request, **kwargs):
        """POST for view SwMuseReport """

        to_log(request)

        response = self.unavailable_request(
                                     error_message="Unavailable POST request",
                                     more_message="Use GET request")
        return response

class SwMuseRun(SwRunViewMixin, generics.RetrieveAPIView):
    """Runs a software under Muse cluster

    'GET  software/muse/run/{name}/{key}' request, after
    'POST software/muse/run/{name}' request

    Remote run under cluster, always as singularity command (if container \
    type is docker or singularity).

    Note : If container type is docker, then the run singularity image must \
    have previously been built from the docker image (as required by ws).

    Chronology to run a software under muse cluster and get the run result :

    I. 'POST software/muse/run/{name}' :

     - Launchs on muse cluster the 'name' software, according to parameters.
     - Returns information required to follow cluster process and get results :
       key value.

    II. 'GET software/muse/state/{name}/{key} :

       Returns cluster job information (IN_PROGRESS, ENDED_OK, ENDED_NOK...).

    If cr is "OK" into "report" of the response to II., then III. can be done.

    III. 'GET software/muse/run/{name}/{key} :

       Returns the run results when ready (when cluster process ended).

    Help while running a software under muse cluster :
    
    A. 'GET software/muse/content/{name}/{key} :

       Returns cluster job content (.zip), to investigate if problems.

    Note :
     - If mail-user given into sbatch_list of POST request then, during \
       process running, the user will receive notifications emails when \
       cluster job state changes according to mail-type value : "ALL" by \
       default ; mail-type can also be given into sbatch_list of POST request.
 
    Note : 'GET software/list' : to list all available softwares.
    """

    serializer_class = serializers.Serializer

    def get_queryset(self):
        return Software.objects.all()

    def get_object(self, name, request):
        sw = Software.objects.get(name=name)
        sw.complete_ini(request)
        return sw

    def get(self, request, **kwargs):
        """GET for view SwMuseRun """

        rsrc = MuseResource()
        response = self.get_remote_run(request=request, rsrc=rsrc, **kwargs)
        return response
        
    def post(self, request, **kwargs):
        """POST for view SwMuseRun """

        rsrc = MuseResource()
        response = self.post_remote_run(request=request, rsrc=rsrc, **kwargs)
        return response

