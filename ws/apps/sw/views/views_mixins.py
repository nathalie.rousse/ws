"""sw.views.views_mixins

Mixins for sw application views

"""

from sw.serializers import CmdSerializer
from sw.serializers import CmdOptionSerializer
from sw.serializers import ReturnedTypeSerializer
from sw.serializers import TodownloadSerializer
from cmn.serializers import get_key_option_values
from cmn.utils.coding import json_py_from_str

class CmdViewMixin(object):
    """Additional methods for views having 'cmd' and 'cmd_option' options """

    def get_cmd_value(self, data):
        """Returns the 'cmd' value

        Reads and adapts the 'cmd' value found in data 

        Default value : ""
        """

        p = CmdSerializer(data=data)
        p.is_valid()
        cmd = p.data['cmd']

        default_cmd = ""
        if cmd is None :
            cmd = default_cmd

        return cmd

    def get_cmd_option_value(self, data):
        """Returns the 'cmd_option' value

        Reads and adapts the 'cmd_option' value found in data 

        Default value : ""
        """

        p = CmdOptionSerializer(data=data)
        p.is_valid()
        cmd_option = p.data['cmd_option']

        default_cmd_option = ""
        
        if cmd_option is None :
            cmd_option = default_cmd_option

        return cmd_option

class ReturnViewMixin(object):
    """Additional methods for views having 'returned_type' and 'todownload' \
       options
    """

    def get_returned_type_value(self, data) :
        """Returns the 'returned_type' value

        Reads and adapts the 'returned_type' value found in data 

        Default value : "stdout"
        """

        p = ReturnedTypeSerializer(data=data)
        p.is_valid()
        returned_type = p.data['returned_type']

        default_returned_type = "stdout"
        if returned_type in ["stdout", "str", "Str", "string", "String",
                             "txt", "Txt", "text", "Text" ] :
            returned_type = "stdout"
        elif returned_type in ['run.zip', 'all.zip', 'folder.zip'] :
            returned_type = "run.zip"
        elif returned_type in ["stdout.txt", "wsout.txt", "out.txt"] :
            returned_type = "stdout.txt"
        else :
            returned_type = default_returned_type
        return returned_type

    def get_todownload_value(self, data, returned_type):
        """Returns as a bool the 'todownload' value

        Reads and adapts the 'todownload' value found in data 

        Default value : False
        """

        p = TodownloadSerializer(data=data)
        p.is_valid()
        todownload = p.data['todownload']

        default_v = False
        v = default_v
        if todownload in ['yes', 'Yes', 'true', 'True'] :
            v = True
        elif todownload in ['no', 'No', 'false', 'False'] :
            v = False
        if (returned_type != "run.zip") and (returned_type != "stdout.txt") :
            v = False

        return v

class InputFilesViewMixin(object):
    """Additional methods for views having 'file' and 'files_order' options """

    def get_files(self, data):
        """Returns the list of input files from 'file' option """

        files = get_key_option_values(data=data, key='file')
        return files

    def get_files_order(self, data) :
        """Returns the list of the file names from 'files_order' option """

        files_order = json_py_from_str(data['files_order'])
        return files_order

class ClusterConfigViewMixin(object):
    """Additional methods for views having cluster config options """

    def get_sbatch_list(self, data) :
        """Returns the 'sbatch_list' option """

        sbatch_list = []
        if 'sbatch_list' in data.keys() :
            sbatch_list = data['sbatch_list']
            sbatch_list = json_py_from_str(data['sbatch_list']) # ws_072021
                                 # ws_072021 -F 'sbatch_list=["--time=8:00:00"]'
        return sbatch_list

    def get_module_list(self, data) :
        """Returns the 'module_list' option """

        module_list = []
        if 'module_list' in data.keys() :
            module_list = data['module_list']
            module_list = json_py_from_str(data['module_list']) # ws_072021
        return module_list
