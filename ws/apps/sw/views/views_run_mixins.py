"""sw.views.views_run_mixins

Mixins for sw application views running softwares

"""

import os
 
from rest_framework.reverse import reverse
from rest_framework.response import Response

from sw.models import Software
from sw.models_mixins.resource import WsmResource
from sw.models_mixins.workspace import RunWorkspace
from sw.models_mixins.workspace import Workspace
from sw.models_mixins.container import Container
from sw.models import LocalAct
from sw.models import ClusterAct

from cmn.views_mixins import DataViewMixin
from cmn.views_mixins import FileViewMixin
from sw.views.views_mixins import CmdViewMixin
from sw.views.views_mixins import ReturnViewMixin
from sw.views.views_mixins import InputFilesViewMixin
from sw.views.views_mixins import ClusterConfigViewMixin
from load.views_mixins import DownloadViewMixin
from cmn.views_mixins import UnavailableRequestViewMixin
from acs.views.views_mixins import AccessibilityViewMixin

from conf.config import DOWNLOADS_HOME_SW

from cmn.utils.folder_file import save_uploaded_file
from cmn.utils.folder_file import is_zip_file
from cmn.utils.folder_file import unzip_file
from cmn.utils.folder_file import set_all_user_rights

from cmn.utils.log import to_log
from cmn.utils.errors import complete_error_message
from cmn.utils.errors import get_exception_info


class SwRunViewMixin(UnavailableRequestViewMixin,
                     AccessibilityViewMixin,
                     ReturnViewMixin, CmdViewMixin, DataViewMixin,
                     DownloadViewMixin, FileViewMixin, InputFilesViewMixin,
                     ClusterConfigViewMixin): # FormatViewMixin
    """Additional methods for views running software """


    def get_sw(self, request, **kwargs) :
        """gets and returns the Software with name from argument"""

        #print("get_sw de SwRunViewMixin")

        (error_case, errortype, errordetails, errormsg) = \
                                             (False, None, None, "") # default
        sw_name = kwargs['name']
        sw = None
        try :
            sw = self.get_object(name=sw_name, request=request)
        except Software.DoesNotExist as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)
            errormsg = "Software with "+sw_name+" as name does not exist"
        except Exception as e :
            error_case = True
            (errortype, errordetails) = get_exception_info(e)

        return (sw, error_case, errortype, errordetails, errormsg)


    def prepare_files(self, data, wk) :
        """Prepares files received into POST data by installing them into wk

        Input :
        - wk : Workspace. Careful : wk.run_path must exist !
        - Files 'file' into data :
          Some input files that will be copied, and then unzipped if zip file.
          Operations in overwrite mode.
        - files_order (list) into data :
          Ordered list of the file names giving the order to treat them

        Does for each file (in order defined by files_order) :
         - gets and installs it under wk.run_home (overwrites if necessary)
         - unzip it if zip file (overwriting)

        Output  :
        - returns ordered_filename_list : ordered list of the names of the
          input files that have been taken into account
        """

        def get_ordered_files(data) :
            """Returns the input files and ordered_indices

            files will have to be treated as the following order :
            files[i] for i in ordered_indices.

            information comes from 'file' and 'files_order' options of data
            """

            if 'file' in data.keys() :
                files = self.get_files(data)

                for f in files :
                    if type(f) == str :
                        errormsg = "Error while getting files : "
                        errormsg += +"'"+f+"'" + " is 'str' instead of 'file'"
                        raise Exception(errormsg)
                fnames = [f.name for f in files]
                ordered_indices = list() # files[i]
                files_order = list()
                use_files_order = False
                if 'files_order' in data.keys() :
                    files_order = self.get_files_order(data)
                    if len(files_order) > 0 :
                        use_files_order = True
                if use_files_order :
                    for fn in files_order :
                        i = None
                        for j,fname in enumerate(fnames) :
                            if fname == fn :
                                i = j
                        if i is not None :
                            ordered_indices.append(i)
                else :
                    ordered_indices = [i for i,fname in enumerate(fnames)]
                return (files, ordered_indices)

        try :
            ordered_filename_list = list()
            if 'file' in data.keys() :
                (files, ordered_indices) = get_ordered_files(data)
                for i in ordered_indices :
                    f = files[i]
                    file_path = os.path.join(wk.run_path, f.name)
                    cr_ok = save_uploaded_file(imulf=f, file_path=file_path)
                    set_all_user_rights(file_path) # __wsdev__ Careful :
                                                   # file_path FORCED to 755 
                    if not cr_ok :
                        errormsg = "Error while saving uploaded file named "
                        errormsg += f.name
                        raise Exception(errormsg)
                    if is_zip_file(file_path=file_path)  :
                        cr_ok = unzip_file(zip_file_path=file_path,
                                           path=wk.run_path)
                        if not cr_ok :
                            errormsg = "Error while unzipping uploaded zip "
                            errormsg += "file named " + f.name
                            raise Exception(errormsg)
                    ordered_filename_list.append(f.name)
        except :
            raise
        return ordered_filename_list


    def build_response(self, base_url, returned_type, todownload, run_path,
                       report) :

        def make_download_response(file_path, dirname) :
            download_document = self.build_download_folder(
                                         file_path=file_path, dirname=dirname)
            label = "The response content can be downloaded at"
            (download_url, more_text) = self.get_download_url(
                                          base_url=base_url,
                                          download_document=download_document)
            response = self.download_response(url=download_url, label=label,
                                              more_text=more_text)
            return response

        rwk = RunWorkspace.get_run_workspace(run_path=run_path)
        if returned_type == "run.zip" :
            zip_folder_path = rwk.make_run_zip(report=report)
            if todownload :
                response = make_download_response(file_path=zip_folder_path,
                                                  dirname=DOWNLOADS_HOME_SW)
            else :
                response = self.zipfile_response(file_path=zip_folder_path)

        elif returned_type == "stdout.txt" :
            stdout_file_path = rwk.get_stdout_file_path()
            if todownload :
                response = make_download_response(file_path=stdout_file_path,
                                                  dirname=DOWNLOADS_HOME_SW)
            else :
                response = self.txtfile_response(file_path=stdout_file_path)

        else : # returned_type=="stdout", else
            stdout_file_path = rwk.get_stdout_file_path()
            txt_stdout = Container.get_txt_stdout(stdout_file_path)
            info = {"stdout":txt_stdout, "report":report}
            context = dict()
            context['info'] = info
            response = Response(context)
        return response

    def make_get_remote_urls(self, cluact, request) :
        """returns urls GET muse/state,content,run (value and description) """

        from sw.urls import url_desc
        name = 'sw-software-muse-state-get'
        state_url = reverse(name, args=[cluact.name, cluact.key],
                            request=request)
        state_url_desc = url_desc(name)
        name = 'sw-software-muse-content-get'
        content_url = reverse(name, args=[cluact.name, cluact.key],
                              request=request)
        content_url_desc = url_desc(name)
        name = 'sw-software-muse-run-get'
        run_url = reverse(name, args=[cluact.name, cluact.key],
                          request=request)
        run_url_desc = url_desc(name)

        return (state_url, state_url_desc,
                content_url, content_url_desc,
                run_url, run_url_desc)

    def filtered_response(self, response, error_case,
                          errormsg, errordetails, errortype) :

        if error_case : # common to error cases
            msg = "Unable to satisfy the request"
            errormsg = complete_error_message(errormsg, msg)
            context = dict()
            context['error'] = errormsg
            context['more'] = errordetails
            context['type'] = errortype
            response = Response(context)

        return response

    def get_local_run(self, request, **kwargs) :
        """Get to run software (local run case)

        Unavailable request returning an error
        """

        to_log(request)
        error_message="Unavailable GET request"
        more_message="Use POST request"
        response = self.unavailable_request(error_message, more_message)
        return response

    def post_local_run(self, request, AUTH_REQUIRED=True, **kwargs) :
        """POST to run software (local run case)

        Runs 'name' software and returns result, according to parameters.

        Local run always as singularity command (if container type is docker \
        or singularity).

        Access : jwt parameter required if AUTH_REQUIRED=True

        Response :

        ** "todownload"="no" case :

           ** "returned_type"="stdout" case :

              { "info": { 
                  "stdout":"9.0",
                  "report":{
                    "cr":"OK",
                    "name":"sw_example",
                    "request" information
                  }
                }
              }

           ** "returned_type"="stdout.txt" case :
              txt file (containing stdout)

           ** "returned_type"="run.zip" case :
              zip file (including report information)

        ** "todownload"="yes" case :
           text explaining how to download response (by a get request)

        """

        to_log(request)

        rsrc = WsmResource() # local resource

        (sw, error_case, errortype, errordetails, errormsg) = \
                                                self.get_sw(request, **kwargs)

        (user_name, level, response) = (None, None, None) # default
        if not error_case :
            if AUTH_REQUIRED :
                try :
                    (user_name, level) = self.control_access(ocn=sw.ocn,
                                                             request=request)
                except Exception as e :
                    error_case = True
                    (errortype, errordetails) = get_exception_info(e)
            else :
                level = self.access_level_max_value()

        #print("user ", user_name, ", level ", level) # ws_072021

        if not error_case :
            try :
                data = self.request_data(request)
                download_base_url = self.get_download_base_url(request)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        if not error_case :
            if not self.access_files_allowed(level) :
                if 'file' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "Sending file is not allowed"
                elif 'files_order' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "files_order parameter is not allowed"

        if not error_case :
            try :
                cmd = self.get_cmd_value(data)
                cmd_option = self.get_cmd_option_value(data)
                returned_type  = self.get_returned_type_value(data)
                todownload = self.get_todownload_value(data, returned_type)
                cn = Container.create(sw=sw, rsrc=rsrc)

            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        if not error_case :
            try :
                wk = Workspace.create(rsrc=rsrc)
                run_path = wk.make_run_path()
                ordered_filename_list = self.prepare_files(data, wk)

                retcode = cn.execute(wk=wk, rsrc=rsrc,
                                     user_cmd=cmd, cmd_option=cmd_option)

                report = LocalAct.begin_report(name=sw.name, cmd=cmd,
                                  cmd_option=cmd_option,
                                  ordered_filename_list=ordered_filename_list,
                                  returned_type=returned_type,
                                  todownload=todownload)
                report["cr"] = "OK"
                response = self.build_response(base_url=download_base_url,
                                               returned_type=returned_type,
                                               todownload=todownload,
                                               run_path=wk.get_run_path(),
                                               report=report)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        return self.filtered_response(response, error_case,
                                      errormsg, errordetails, errortype)

    def post_remote_run(self, request, rsrc, AUTH_REQUIRED=True, **kwargs) :
        """POST to run software (remote cluster run case)

        Available cluster case : muse

        Remote launchs on muse cluster the 'name' software, according to \
        parameters. Returns information required to follow cluster process \
        and get results : key value.

        Remote run under cluster, always as singularity command (if container \
        type is docker or singularity).

        Access : jwt parameter required if AUTH_REQUIRED=True (rights will \
        depend on it).

        Response :
        {
          "msg": text about how to go on (to get cluster job state, results)
          "url": URL value, for following get request(s)
        }

        Note : 
        During process running, notifications emails will be sent to \
        mail-user when cluster job state changes according to mail-type \
        value ("ALL", "END"...).
        mail-user and mail-type can be given into sbatch_list of POST request.

        """

        to_log(request)

        # identifies software to be cluster launched
        (sw, error_case, errortype, errordetails, errormsg) = \
                                                self.get_sw(request, **kwargs)

        (user_name, level, response) = (None, None, None) # default
        if not error_case :
            if AUTH_REQUIRED :
                try :
                    (user_name, level) = self.control_access(ocn=sw.ocn,
                                                             request=request)
                except Exception as e :
                    error_case = True
                    (errortype, errordetails) = get_exception_info(e)
            else :
                level = self.access_level_max_value()

        if not error_case :
            if "key" in kwargs.keys() :
                error_case = True
                errortype = ""
                # software/muse/run/{name}
                errordetails = "Use POST " + request.path
                errormsg = "There shouldn't be key value in POST request"

        #print("user ", user_name, ", level ", level) # ws_072021

        if not error_case :
            try :
                name = sw.get_name()
                data = self.request_data(request)
                download_base_url = self.get_download_base_url(request)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        if not error_case :
            if not self.access_files_allowed(level) :
                if 'file' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "Sending file is not allowed"
                elif 'files_order' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "files_order parameter is not allowed"

        if not error_case :
            if not self.access_cluster_conf_allowed(level) :
                if 'sbatch_list' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "sbatch_list parameter is not allowed"
                elif 'module_list' in data.keys() :
                    error_case = True
                    errortype = ""
                    errordetails = "You don't have required rights"
                    errormsg = "module_list parameter is not allowed"

        if not error_case :
            try :
                cmd = self.get_cmd_value(data)
                cmd_option = self.get_cmd_option_value(data)
                returned_type  = self.get_returned_type_value(data)
                todownload = self.get_todownload_value(data, returned_type)
                sbatch_list = self.get_sbatch_list(data)
                module_list = self.get_module_list(data)
                cn = Container.create(sw=sw, rsrc=rsrc)
            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        if not error_case :
            try :
                wk = Workspace.create(rsrc=rsrc)
                run_path = wk.make_run_path()
                ordered_filename_list = self.prepare_files(data, wk)

                cluact = ClusterAct()
                cluact.ini_save(name=name)

                dict_request = {
                               "cmd": cmd,
                               "cmd_option":cmd_option,
                               "ordered_filename_list":ordered_filename_list,
                               "returned_type":returned_type,
                               "todownload":todownload,
                               "sbatch_list":sbatch_list,
                               "module_list":module_list,
                               "download_base_url":download_base_url,
                               "jwt":"__todo__",
                               }
                dict_wk =      {
                               "home_path":wk.home_path,
                               "run_path":wk.run_path,
                               }
                dict_process = {
                               "jobname":cluact.define_jobname(name)
                               }
                dict_rsrc =    {
                               "identity":"muse"
                               }
                cluact.update_save_from(request=dict_request, wk=dict_wk,
                                        rsrc=dict_rsrc, process=dict_process)
                sw_cmd = cn.make_remote_cmd(run_path=wk.run_path, rsrc=rsrc,
                                        user_cmd=cmd, cmd_option=cmd_option)
                cluster_config = {
                                 "jobname":dict_process["jobname"],
                                  "sbatch_list":dict_request["sbatch_list"],
                                  "module_list":dict_request["module_list"],
                                 }
                (retcode, sbatch_msg_stdout, sbatch_msg_stderr) = \
                            rsrc.cluster_launch(wk=wk, sw_cmd=sw_cmd,
                                                cluster_config=cluster_config)

                ret = self.make_get_remote_urls(cluact, request)
                (state_url, state_url_desc, content_url, content_url_desc,
                 run_url, run_url_desc) = ret
                urls_dict = { state_url : state_url_desc,
                              content_url : content_url_desc,
                              run_url : run_url_desc }

                email_text = "Notifications will be sent during process running if mail-user given into sbatch_list option."
                text = "GET " +state_url+ " request to get cluster job state, while waiting for results (the GET "+run_url+" request to get results will be possible once job finished). Keep the software name value=" +cluact.name+ " and the key value=" +cluact.key+ " in order to be able to follow your POST request. " +email_text+ " (__todo__: add information about temps imparti...)"

                context = {'msg':text, "url":state_url, "urls":urls_dict}
                response = Response(context)

            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        return self.filtered_response(response, error_case,
                                      errormsg, errordetails, errortype)

    @classmethod
    def build_state_context(cls, job_id, cr,
                            state_url, content_url, run_url) :
        """Builds get_remote_state response context in some cases

           Note : Extracted to be easierly shared with specialized applications
        """
        if job_id is not None :

            if cr == "ENDED_OK" : 
                report_cr = "OK"
                msg = "End of job."
                msg += " You can now get results by"
                msg += " GET " +run_url+ " request"
                msg += " or ask for content (to investigate) by"
                msg += " GET " +content_url+ " request."
                url_value = run_url
            elif cr == "IN_PROGRESS" :
                report_cr = "WAIT"
                msg = "Job in progress."
                msg += " You can try again by"
                msg += " GET " +state_url+ " request"
                msg += " or ask for content (to investigate) by"
                msg += " GET " +content_url+ " request."
                url_value = state_url
            else : # "ENDED_NOK" :
                report_cr = "NOK"
                msg = "Job failed (JOB CR:"+cr+", STATE:"+state+")."
                msg += " You can ask for content (to investigate) by"
                msg += " GET " +content_url+ " request."
                url_value = content_url
        else : # job_id is None 
            report_cr = "WAIT"
            msg = "Unknown-undefined JOB ID."
            msg += " You can try again by"
            msg += " GET " +state_url+ " request"
            msg += " or ask for content (to investigate) by"
            msg += " GET " +content_url+ " request."
            url_value = state_url

        return (report_cr, msg, url_value)

    def get_remote_state(self, request, rsrc, **kwargs) :
        """GET to get cluster job information as text, after post_remote_run

        Response :

        In "cr"="OK" or "NOK" or "WAIT" case :

          {  "info": { 
                 "report": {
                     "cr": value "OK" or "NOK" or "WAIT"
                     "name": software name,
                     "request", "rsrc", "process" information
                 }
             }
             "msg": text where how to go on
             "url": URL value (probable following get request ; see also msg)
          }

        Else : { "error": error text, "more":[...], "type":error type }

        """

        to_log(request)

        response = None # default

        # identifies name (software name) and key 
        (sw, error_case, errortype, errordetails, errormsg) = \
                                                self.get_sw(request, **kwargs)
        if not error_case :
            if "key" not in kwargs.keys() :
                error_case = True
                errortype = ""
                # software/muse/state/{name}/{key}
                errordetails = "Use GET " + request.path
                errormsg = "key value missing in GET request"

        if not error_case :
            name = sw.name
            key = kwargs['key']

            try :

                # actions
                cluact = ClusterAct.objects.get(key=key)
                dict_process = cluact.update_cluster_process(rsrc=rsrc)
                cluact.update_save_from(process=dict_process)

                I = cluact.extract_from_info()
                P = cluact.extract_from_priv()

                job_id = cluact.get_process_job_id_from(I,P)
                cr = cluact.get_process_latest_cr_from(I,P)
                state = cluact.get_process_latest_state_from(I,P)

                report = cluact.begin_report(I)

                ret = self.make_get_remote_urls(cluact, request)
                (state_url, state_url_desc, content_url, content_url_desc,
                 run_url, run_url_desc) = ret
                urls_dict = { state_url : state_url_desc,
                              content_url : content_url_desc,
                              run_url : run_url_desc }

                (report_cr, msg, url_value) = self.build_state_context(job_id,
                                           cr, state_url, content_url, run_url)
                report["cr"] = report_cr
                context = dict()
                context["info"] = { "report":report, }
                context["msg"] = msg
                context["url"] = url_value
                context["urls"] = urls_dict
                response = Response(context)

            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        return self.filtered_response(response, error_case,
                                      errormsg, errordetails, errortype)

    def get_remote_content(self, request, rsrc, **kwargs) :
        """GET to get cluster job information as file, after post_remote_run

        Response : run_path folder zipped as is.

        Or in error case :
           { "error": error text, "more":[...], "type":error type }

        Note : what if run_path being modified while zipping ?
        """

        to_log(request)

        response = None # default

        # identifies name (software name) and key 
        (sw, error_case, errortype, errordetails, errormsg) = \
                                                self.get_sw(request, **kwargs)
        if not error_case :
            if "key" not in kwargs.keys() :
                error_case = True
                errortype = ""
                # software/muse/run/{name}/{key}
                errordetails = "Use GET " + request.path
                errormsg = "key value missing in GET request"

        if not error_case :
            name = sw.name
            key = kwargs['key']

            try :

                # actions
                cluact = ClusterAct.objects.get(key=key)
                dict_process = cluact.update_cluster_process(rsrc=rsrc)
                cluact.update_save_from(process=dict_process)

                I = cluact.extract_from_info()
                P = cluact.extract_from_priv()

                run_path = cluact.get_wk_run_path_from(I,P)
                if run_path is None :
                    msg = "unavailable inputs workspace run_path"
                    raise Exception(msg)
                rwk = RunWorkspace.get_run_workspace(run_path=run_path)
                zip_folder_path = rwk.make_run_zip()
                response = self.zipfile_response(file_path=zip_folder_path)

            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        return self.filtered_response(response, error_case,
                                      errormsg, errordetails, errortype)

    def get_remote_run(self, request, rsrc, **kwargs) :
        """GET to get results after post_remote_run (and get_remote_state)

        Request available only if 'cr':'OK' into 'report' of get_remote_state.

        Response :

        In "cr"="OK" case :

            Results according to parameters of the post_remote_run request.

        In "cr"="NOK" or "WAIT" case or else :

            {"error": error text, "more":[...], "type":error type}

        """

        to_log(request)

        response = None # default

        # identifies name (software name) and key 
        (sw, error_case, errortype, errordetails, errormsg) = \
                                                self.get_sw(request, **kwargs)
        if not error_case :
            if "key" not in kwargs.keys() :
                error_case = True
                errortype = ""
                # software/muse/run/{name}/{key}
                errordetails = "Use GET " + request.path
                errormsg = "key value missing in GET request"

        if not error_case :
            name = sw.name
            key = kwargs['key']

            try :

                # actions
                cluact = ClusterAct.objects.get(key=key)
                dict_process = cluact.update_cluster_process(rsrc=rsrc)
                cluact.update_save_from(process=dict_process)

                I = cluact.extract_from_info()
                P = cluact.extract_from_priv()

                job_id = cluact.get_process_job_id_from(I,P)
                cr = cluact.get_process_latest_cr_from(I,P)
                state = cluact.get_process_latest_state_from(I,P)

                report = cluact.begin_report(I)

                next_url = reverse('sw-software-muse-state-get',
                                    args=[cluact.name, cluact.key],
                                    request=request)
                next_text = "GET " +next_url+ " to get cluster job state"
                next_text += " and continue"
                available_text = "Request available only"
                available_text += " if 'cr':'OK' into 'report'."

                if job_id is not None :

                    if cr == "ENDED_OK" : # __wsdev__ Note :
                           # done again at each new request once cr=="ENDED_OK" 
                        returned_type = cluact.get_returned_type_from(I,P)
                        todownload = cluact.get_todownload_from(I,P)
                        download_base_url = cluact.get_download_base_url_from(
                                                                          I,P)
                        run_path = cluact.get_wk_run_path_from(I,P)
                        if run_path is None :
                            msg = "unavailable inputs workspace run_path"
                            raise Exception(msg)
                        rwk = RunWorkspace.get_run_workspace(run_path=run_path)
                        stderr_file_path = rwk.get_stderr_file_path()
                        error_msg = Container.get_error_msg(
                                            stderr_file_path=stderr_file_path)
                        if error_msg != "" :
                            msg = "execution error found into " +rwk.run_path
                            msg += " : "+error_msg
                            raise Exception(msg)

                        report["cr"] = "OK"
                        response = self.build_response(
                                             base_url=download_base_url,
                                             returned_type=returned_type,
                                             todownload=todownload,
                                             run_path=run_path, report=report)

                    else :
                        if cr == "IN_PROGRESS" :
                            msg = available_text
                            msg += " cr='WAIT' (JOB in progress). " + next_text
                            raise Exception(msg)
                        else : # "ENDED_NOK" :
                            msg = available_text
                            msg += " cr='NOK',"
                            msg += " Failure (JOB CR:"+cr+", STATE:"+state+")"
                            msg += " " + next_text
                            raise Exception(msg)

                else : # job_id is None 
                    msg = available_text
                    msg += " cr='WAIT' (unknown-undefined JOB ID) " + next_text
                    raise Exception(msg)

            except Exception as e :
                error_case = True
                (errortype, errordetails) = get_exception_info(e)

        return self.filtered_response(response, error_case,
                                      errormsg, errordetails, errortype)

