"""sw.models_mixins.container
  
Container

"""

import os

from cmn.utils.process import local_call_subprocess
from cmn.utils.folder_file import is_a_file
from cmn.utils.folder_file import get_txt_from_file
from cmn.utils.folder_file import set_all_user_rights

class Container(object):
    """Container as Singularity

    Attributes :
    - image : image file name

    Container is always of 'singularity' type, whereas Software available
    types are : 'docker', 'singularity'.

    Important :
    When recording a Software, as a 'docker' container, into ws database,
    a singularity image file has to be built from the docker container and 
    installed as expected by ws (cf 'get_singularity_image_file_name_from').

    Note : The image file full path depends on the used resource
    (request /run /muse/run...)
    """

    def __init__(self, image) :
        self.image = image

    @classmethod
    def create(cls, sw, rsrc) :
        """Creates a Container for sw software on rsrc resource

        The container is or comes from sw container, depending on sw container
        type (docker, singularity).

        """

        try :
            exec_image = cls.get_singularity_image(sw=sw, rsrc=rsrc)
            cn = Container(image=exec_image)
        except :
            raise
        return cn

    @classmethod
    def get_singularity_image(cls, sw, rsrc) :
        """Returns the name of the singularity image file about sw software

        The singularity image file is or comes from sw container, depending on
        sw container type :
        - If sw container type is 'singularity', then the singularity image
          file is the sw container one.
        - If sw container type is 'docker', then the singularity image file is
          the one built from the sw container docker image.

        """
        def get_image_file_path(simg_home, image_file_name) :
            """Returns the full path of a singularity image file 
    
            Singularity image files are located into the simg_home folder
            """
    
            return os.path.join(simg_home, image_file_name)
    
        def get_singularity_image_file_name_from(docker_image_name) :
            """Defines and returns the name of the singularity image file \
            built from docker_image_name docker container
            """

            return ("."+docker_image_name+".simg")

        exec_image = None # default
        try :
            (cn_type, cn_image) = sw.get_info_cn()

            if cn_type == "singularity" :
                exec_image = cn_image
                image_file_path = get_image_file_path(
                                         simg_home=rsrc.get_local_simg_home(),
                                         image_file_name=exec_image)
                if not is_a_file(image_file_path) :
                    msg = "Singularity image file "+exec_image+" not found"
                    msg += " as "+image_file_path
                    raise Exception(msg)

            elif cn_type == "docker" :
                exec_image = get_singularity_image_file_name_from(
                                                   docker_image_name=cn_image)
                image_file_path = get_image_file_path(
                                         simg_home=rsrc.get_local_simg_home(),
                                         image_file_name=exec_image)
                if not is_a_file(image_file_path) :
                    msg = "Singularity image file "+exec_image
                    msg += " from docker image "+cn_image+" not found"
                    msg += " as "+image_file_path
                    raise Exception(msg)

            else : # impossible
                raise Exception("unavailable container type")

        except :
            raise

        return exec_image

    def execute(self, wk, rsrc, user_cmd, cmd_option) :
        """Executes container command under workspace run_path
        
        The command is defined from user_cmd and cmd_option.

        Files produced into workspace : stdout file, stderr file, cmd file.

        Looks at the stderr file to detect error case.

        Note : preparation has to be called before (folder and files copy...)

        """

        try :
            cn_cmd = self.make_local_cmd(run_path=wk.run_path, rsrc=rsrc,
                          user_cmd=user_cmd, cmd_option=cmd_option)
            sw_cmd = "cd "+wk.run_path+" ; "+cn_cmd

            stdout_file_path = wk.get_stdout_file_path()
            stderr_file_path = wk.get_stderr_file_path()
            cmd_file_path = wk.get_cmd_file_path()

            retcode =  local_call_subprocess(cmd=sw_cmd,
                                         stderr_file_path=stderr_file_path,
                                         stdout_file_path=stdout_file_path,
                                         cmd_file_path=cmd_file_path)

            error_msg = self.get_error_msg(stderr_file_path=stderr_file_path)
            # Possible : run OK whereas error_msg != ""
            #if error_msg != "" : 
            #    msg = "error while executing "+sw_cmd+" into "+wk.run_path
            #    msg += " : "+error_msg
            #    raise Exception(msg)
        except :
            raise

        return retcode

    @classmethod
    def get_error_msg(cls, stderr_file_path) :

        txt_stderr = get_txt_from_file(stderr_file_path)[:-1] # __todo__ (ok?)

        # " " and "\n" not taken into account
        error_msg = ""
        txt = txt_stderr.replace(" ", "")
        txt = txt.replace("\n", "")
        if len(txt) >= 1 : # not empty
            # __todo__ : at least one warning word => all txt accepted !!?!!
            if ("Warning" not in txt) and ("warning" not in txt) and ("WARNING" not in txt) : 
                error_msg = txt_stderr
        return error_msg

    @classmethod
    def get_txt_stdout(cls, stdout_file_path) :
        txt_stdout = get_txt_from_file(stdout_file_path)[:-1] # __todo__ (ok?)
        return txt_stdout

    @classmethod
    def make_ws_script_file(cls, run_path, user_cmd) :
        """Builds script file to be run by container (in order to run user_cmd)

        Returns the ws_cmd command to call the built script file

        Note : mount $PWD:/WS to run ws_cmd
        """

        ws_script_file_path = os.path.join(run_path, "ws_command.sh")
        f = open(ws_script_file_path, "w")
        txt =  "#!/bin/bash" + "\n"
        txt += "cd /WS" + "\n"
        txt += "PATH=/WS:$PATH" + "\n"
        txt += user_cmd + "\n"
        f.write(txt)
        f.close()
        set_all_user_rights(ws_script_file_path)
        ws_cmd = "/WS/ws_command.sh"
        return ws_cmd

###############################################################################
#__wsdev__ note pour requete parallelisation
#
# Ordre appel dans cluster_script :
#   mpirun singularity exec ..commande..
# et non pas
#   singularity exec mpirun ..commande..
# Avec compatibilite entre mpi dans et hors container (cf 'module load ...')
#
# Essai qui a ete effectue :
#
#   Dans def make_cmd(cls, run_path, image_path, user_cmd, cmd_option) :
#   - a la place de (passer par script) :
#       cls.make_ws_script_file(run_path=run_path, user_cmd=user_cmd)
#   - ligne de commande sans passer par script :
#       ws_cmd = user_cmd
#       ws_cmd_option = "--no-home --bind $PWD:/WS --pwd /WS " + cmd_option
#       cn_singularity_cmd = "SINGULARITYENV_PREPEND_PATH=/WS"
#       cn_singularity_cmd += " singularity exec "+ws_cmd_option
#       cn_singularity_cmd += " "+image_path+" "+ws_cmd
#
#   => ...pb!!! vle.simg
#               POST avec cmd=echo $VLE_HOME => "stdout":"/home/nrousse/vle"
#                                    au lieu de "stdout":"/VLE/vle_home"
#
#La commande mpirun doit etre disponible sur la machine hote, c'est cette commande qui va dispatcher le conteneur sur les CPU physiques de la machine. De plus il faut que la version locale de MPI soit compatible avec celle utilisee pour compiler le fichier dans le conteneur.
#
# When running code within a Singularity container, we don’t use the MPI executables stored within the container (i.e. we DO NOT run singularity exec mpirun -np <numprocs> /path/to/my/executable). Instead we use the MPI installation on the host system to run Singularity and start an instance of our executable from within a container for each MPI process. Without Singularity support in an MPI implementation, this results in starting a separate Singularity container instance within each process. This can present some overhead if a large number of processes are being run on a host. Where Singularity support is built into an MPI implementation this can address this potential issue and reduce the overhead of running code from within a container as part of an MPI job.
#
###############################################################################


    @classmethod
    def make_cmd(cls, run_path, image_path, user_cmd, cmd_option) :
        """Makes and returns command for singularity image_path

        Note : cmd_option is given as singularity syntax
        """

        ws_cmd = cls.make_ws_script_file(run_path=run_path, user_cmd=user_cmd)
        ws_cmd_option = "--no-home --bind $PWD:/WS " + cmd_option
        cn_singularity_cmd = "singularity exec "+ws_cmd_option
        cn_singularity_cmd += " "+image_path+" "+ws_cmd
        return cn_singularity_cmd

    def make_local_cmd(self, run_path, rsrc, user_cmd, cmd_option) :
        """Makes and returns local command for singularity container"""

        local_image_path = os.path.join(rsrc.get_local_simg_home(), self.image)
        local_cmd = self.make_cmd(run_path=run_path,
                                  image_path=local_image_path,
                                  user_cmd=user_cmd, cmd_option=cmd_option)
        return local_cmd

    def make_remote_cmd(self, run_path, rsrc, user_cmd, cmd_option) :
        """Makes and returns remote command for singularity container"""

        remote_image_path = os.path.join(rsrc.get_remote_simg_home(),
                                         self.image)
        remote_cmd = self.make_cmd(run_path=run_path,
                                   image_path=remote_image_path,
                                   user_cmd=user_cmd, cmd_option=cmd_option)
        return remote_cmd

