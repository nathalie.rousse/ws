"""sw.models_mixins.resource
  
Calcul resources

Methods in remote cases suppose some mounts between the ws and remote machines

See install/*/install.txt, conf/config.py

"""

import os
import stat

from conf.config import WSM_LOCAL_REQUESTS_HOME
from conf.config import WSM_LOCAL_SIMG_HOME
from conf.config import MUSE_LOCAL_REQUESTS_HOME
from conf.config import MUSE_LOCAL_SIMG_HOME
from conf.config import MUSE_REMOTE_REQUESTS_HOME
from conf.config import MUSE_REMOTE_SIMG_HOME
from conf.config import MUSE_REMOTE_HOST
from conf.config import JOB_ID_FILE_NAME

from cmn.utils.process import remote_call_subprocess_str_mode
from cmn.utils.process import remote_call_subprocess_file_mode

class Resource(object) :
    """Calcul resource

    Available calcul resource identities :
    - 'wsm' ws machine (local resource)
    - 'muse' Muse cluster

    Used to specialize some methods according to the resource
    """

    @classmethod
    def create(cls, identity="wsm") :
        """Creates a resource as defined by identity"""

        if identity == "muse" :
            resource = MuseResource()
        else : # identity == "wsm" or impossible case
            resource = WsmResource()
        return resource

class RemoteResource(Resource) :
    """Remote calcul resource"""

    @classmethod
    def get_remote_path(cls, local_path, local_prefix, remote_prefix):
        """Returns path where local prefix has been replaced by remote one """

        path = os.path.relpath(local_path, local_prefix)
        remote_path = os.path.join(remote_prefix, path)
        return remote_path

    def get_remote_requests_path(self, path):
        """Returns requests path where Local root replaced by Remote root"""

        return self.get_remote_path(local_path=path,
                               local_prefix=self.get_local_requests_home(),
                               remote_prefix=self.get_remote_requests_home())

    def get_remote_simg_path(self, path):
        """Returns simg path where Local root replaced by Remote root"""

        return self.get_remote_path(local_path=subdirectory,
                               local_prefix=self.get_local_simg_home(),
                               remote_prefix=self.get_remote_simg_home())

class ClusterResource(RemoteResource) :
    """Cluster remote calcul resource"""

    @classmethod
    def get_job_id_file_name(cls) :
        return JOB_ID_FILE_NAME

    def try_get_job_id(self, local_run_path) :
        """Tries once to read job_id value, returns it if found and else None
    
        job_id value is read into JOB_ID file
        """

        try :
            job_id = None
            local_job_id_file_path = os.path.join(local_run_path,
                                              self.get_job_id_file_name())
            if os.path.exists(local_job_id_file_path) :
                job_id_file = open(local_job_id_file_path, "r")
                job_id = int(job_id_file.read())
                job_id_file.close()
        except :
            raise
        return job_id

    def get_job_state(self, job_id) : # __todo__ to rewrite
        """Returns cr and state related to job_id

        Outputs :

        - state : job status.
                  Values : RUNNING, RESIZING, SUSPENDED, COMPLETED,
                           CANCELLED, FAILED, TIMEOUT, PREEMPTED, NODE_FAIL.

          RUNNING :   job has received its allocation.
          RESIZING :  ???
          SUSPENDED : job has been cancelled after resources allocation.
          COMPLETED : all processes have finished execution on all nodes.
          CANCELLED : job has been interrupted.
          FAILED :    job has finished with returned code different from 0.
          TIMEOUT :   the delay for the job has been finished.
          PREEMPTED : job has been preempted (its priority increased by admin).
          NODE_FAIL : job has finished because of an error on 1 or more node(s).

        - cr : Values IN_PROGRESS, ENDED_OK, ENDED_NOK.

        state and cr come from "sacct" remote command.

        Note about sacct command :
        If more information is available on the job state than will fit into
        the current field width (for example, the uid that CANCELLED a job),
        then the state will be followed by a "+" (it is possible to increase
        the size of the displayed state).

        """

        def extract_job_line(msg_stdout_job_status, job_id) :
            """Extracts job line from msg_stdout_job_status (from sacct) """

            job_line = ""
            job_id_str = str(job_id) + " "
            lines = msg_stdout_job_status.decode()
            for line in lines.splitlines() :
                if job_id_str in line :
                    job_line = line
                    return job_line
            return job_line

        try :
            remote_host = self.get_remote_host()
            remote_cmd = "sacct --format=jobid,state -j " + str(job_id)
            (retcode_job_status, msg_stdout_job_status,
             msg_stderr_job_status) = remote_call_subprocess_str_mode(
                                                      remote_host=remote_host,
                                                      remote_cmd=remote_cmd)
            job_line = extract_job_line(msg_stdout_job_status, job_id)
            state = ""
            cr = "ENDED_NOK"
            if ("RUNNING" in job_line) :
                state = "RUNNING"
                cr = "IN_PROGRESS"
            elif ("RESIZING" in job_line) :
                state = "RESIZING"
                cr = "IN_PROGRESS"
            elif ("SUSPENDED" in job_line) :
                state = "SUSPENDED"
                cr = "ENDED_NOK"
            elif ("COMPLETED" in job_line) :
                state = "COMPLETED"
                cr = "ENDED_OK"
            elif ("CANCELLED" in job_line) :
                state = "CANCELLED"
                cr = "ENDED_NOK"
            elif ("FAILED" in job_line) :
                state = "FAILED"
                cr = "ENDED_NOK"
            elif ("TIMEOUT" in job_line) :
                state = "TIMEOUT"
                cr = "ENDED_NOK"
            elif ("PREEMPTED" in job_line) :
                state = "PREEMPTED"
                cr = "IN_PROGRESS"
            elif ("NODE_FAIL" in job_line) :
                state = "NODE_FAIL"
                cr = "ENDED_NOK"
            #else :
        except :
            raise
        return (cr, state)

class WsmResource(Resource) :

    @classmethod
    def get_identity(cls) :
        return "wsm"

    @classmethod
    def get_local_requests_home(cls) :
        return WSM_LOCAL_REQUESTS_HOME

    @classmethod
    def get_local_simg_home(cls) :
        return WSM_LOCAL_SIMG_HOME

class MuseResource(ClusterResource) :

    @classmethod
    def get_identity(cls) :
        return "muse"

    @classmethod
    def get_local_requests_home(cls) :
        return MUSE_LOCAL_REQUESTS_HOME

    @classmethod
    def get_local_simg_home(cls) :
        return MUSE_LOCAL_SIMG_HOME

    @classmethod
    def get_remote_requests_home(cls) :
        return MUSE_REMOTE_REQUESTS_HOME

    @classmethod
    def get_remote_simg_home(cls) :
        return MUSE_REMOTE_SIMG_HOME

    @classmethod
    def get_remote_host(cls) :
        return MUSE_REMOTE_HOST

    @classmethod
    def create_cluster_script_file(cls, script_file_path, script_cmd,
                                   cluster_config) :
        """Creates the script file allowing to run script_cmd on muse cluster

        A default configuration, overwritten by the input cluster_config one.

        Inputs :
        - cluster_config['sbatch_list'] list
        - cluster_config['module_list'] list

        Some SLURM parameters :
        (see https://meso-lr.umontpellier.fr/documentation-utilisateurs)

          - job-name : job name
          - account : project name
          - nodes  : number of nodes
          - ntasks : number of tasks
                     ntasks = ntasks-per-node * nodes
          - ntasks-per-node : number of tasks per node
          - ntasks-per-core : number of tasks per core
                              generally ntasks-per-core = 1
          - partition : group of machines where job running
          - mem : memory reserved per node. MegaBytes.
                  Facultatif : if not defined, the whole node memory allocated
          - time : time limit for job running
                   (after, the job is automatically stopped) 
                   HH:MM:SS format (HH : hours, MM : minutes, SS : seconds.
          - mail-user : email address to send notifications

        """

        def get_default_sbatch_list(jobname) :
            nodes = 1
            ntaskspernode = 1
            #default_sbatch_list = [ "--job-name="+jobname,
            #                        "--account=record",
            #                        "--nodes="+str(nodes),
            #                        "--ntasks-per-node="+str(ntaskspernode),
            #                        #(note : ntasks=ntaskspernode*nodes)
            #                        #"--ntasks-per-core=...",
            #                        "--partition=defq",
            #                        #"--mem=1024", # Memory by node (Mo unit)
            #                        "--time=4:00:00",
            #                        "--mail-user=ws179250@gmail.com",
            #                        "--exclusive",
            #                        "--mail-type=ALL", # --mail-type=END
            #                      ]
            default_sbatch_list = [ "--job-name="+jobname,
                                    "--account=record",
                                    "--nodes="+str(nodes),
                                    "--ntasks-per-node="+str(ntaskspernode),
                                    #(note : ntasks=ntaskspernode*nodes)
                                    #"--ntasks-per-core=...",
                                    "--partition=defq",
                                    #"--mem=1024", # Memory by node (Mo unit)
                                    "--time=4:00:00",
                                    "--mail-user=ws179250@gmail.com",
                                    "--exclusive",
                                    "--mail-type=ALL", # --mail-type=END
                                  ]
            return default_sbatch_list

        def get_default_module_list() :
            # __wsdev__
            # enlever module mpi quand software/run (sans parallelisation)
            default_module_list = [ "cv-standard",
                                    "intel/mpi/64/2017.1.132",
                                    ##"intel/mpi/64/2016.3.210",
                                    "singularity/3.5"
                                  ]
            return default_module_list

        def make_script_lines(sbatch_list, module_list) :

            sbatch_lines = [ ("#SBATCH " + v + " \n") for v in sbatch_list]

            other_lines = [ "module purge"+ " \n" ]
            for m in module_list :
                other_lines.append("module load " + m + " \n")
            other_lines.append("echo $SLURM_JOB_ID > " +JOB_ID_FILE_NAME+ " \n")

            #__wsdev__ debut
            #other_lines.append("echo SLURM_JOB_NAME = $SLURM_JOB_NAME\n")
            #other_lines.append("echo SLURM_NTASKS_PER_NODE = $SLURM_NTASKS_PER_NODE\n")
            #other_lines.append("echo SLURM_NTASKS = $SLURM_NTASKS\n")
            #other_lines.append("echo SLURM_JOB_PARTITION = $SLURM_JOB_PARTITION\n")
            #__wsdev__ fin

            lines = ["#!/bin/sh" +"\n"]
            for line in sbatch_lines :
                lines.append(line)
            lines.append("" +"\n")
            for line in other_lines :
                lines.append(line)
            lines.append("" +"\n")
            lines.append(script_cmd +"\n")
            lines.append("" +"\n")

            return lines

        (jobname, user_sbatch_list, user_module_list) = ("ws", [], [])
        if type(cluster_config) is dict :
            keys = cluster_config.keys()
            if 'jobname' in keys :
                jobname = cluster_config['jobname']
            if 'sbatch_list' in keys :
                user_sbatch_list = cluster_config['sbatch_list']
            if 'module_list' in keys :
                user_module_list = cluster_config['module_list']

        # __wsdev__ des controles de user_sbatch_list !!!
        # __wsdev__ des controles de user_module_list !!!

        default_sbatch_list = get_default_sbatch_list(jobname)
        default_module_list = get_default_module_list()

        sbatch_list = default_sbatch_list + user_sbatch_list
        module_list = default_module_list + user_module_list

        lines = make_script_lines(sbatch_list, module_list)

        f = open(script_file_path, 'w')
        for line in lines :
            f.write(line)
        f.close()
        os.chmod(script_file_path,
             stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR |
             stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP | stat.S_IROTH)

    def cluster_launch(self, wk, sw_cmd, cluster_config=None) :
        """Remote cluster launches sw_cmd command

        Launches sw_cmd command (sbatch) under workspace run_path

        Files produced into workspace : stdout file, stderr file, cmd file.

        Looks at output files to detect error case.

        Note : preparation has to be called before (folder and files copy...)

        Inputs :

        - wk, where run_path : run folder (local) where resulting files
        - sw_cmd (remote adapted)
        - cluster_config

        Outputs :

        - retcode : returned code
        - sbatch_msg_stdout : stdout message
        - sbatch_msg_stderr : stderr message

        """

        def sbatch_control_out(retcode, sbatch_msg_stdout, sbatch_msg_stderr) :
            """Makes controls and returns (sbatch_error_case, sbatch_errormsg)
            """

            (sbatch_error_case, sbatch_errormsg) = (False, "") # default
            if sbatch_msg_stderr != "" :
                msg_stderr = filter_sbatch_msg_stderr(sbatch_msg_stderr)
                if msg_stderr == "" :
                    sbatch_error_case = True
                    sbatch_errormsg = msg_stderr
            return (sbatch_error_case, sbatch_errormsg)

        def filter_sbatch_msg_stderr(msg_stderr) :
            if msg_stderr != "" :
                filtered_stderr = ""
                msg = msg_stderr.split("\n")
                for line in msg :
                    if line == '' :
                        pass
                    elif "debug" in line or "Debug" in line :
                        pass
                    elif "DEBUG" in line :
                        pass
                    elif "warning" in line or "Warning" in line :
                        pass
                    elif "WARNING" in line :
                        pass
                    else :
                        filtered_stderr = filtered_stderr + line
            return filtered_stderr

        try :
            local_run_path = wk.run_path
            remote_run_path = self.get_remote_requests_path(
                                                          path=local_run_path)
            remote_stdout_file_path = wk.get_remote_stdout_file_path(
                                              remote_run_path=remote_run_path)
            remote_stderr_file_path = wk.get_remote_stderr_file_path(
                                              remote_run_path=remote_run_path)
            local_cmd_file_path = wk.get_cmd_file_path()
            local_script_file_path = os.path.join(
                                          local_run_path, "cluster_script.sh")
            self.create_cluster_script_file(
                             script_file_path=local_script_file_path,
                             script_cmd=sw_cmd, cluster_config=cluster_config)
            remote_script_file_path = os.path.join(
                                         remote_run_path, "cluster_script.sh")
            local_cluster_stderr_file_path = os.path.join(
                                         local_run_path, "cluster_stderr.txt")
            local_cluster_stdout_file_path = os.path.join(
                                         local_run_path, "cluster_stdout.txt")
            cmd = "cd " + remote_run_path + ";"
            cmd = cmd + " sbatch "
            cmd = cmd + " -e " +remote_stderr_file_path
            cmd = cmd + " -o " +remote_stdout_file_path
            cmd = cmd + " " + remote_script_file_path
        
            ret = remote_call_subprocess_file_mode(
                        remote_host=self.get_remote_host(),
                        remote_cmd=cmd,
                        local_run_path=local_run_path,
                        local_stderr_file_path=local_cluster_stderr_file_path,
                        local_stdout_file_path=local_cluster_stdout_file_path,
                        local_cmd_file_path=local_cmd_file_path)
            retcode = ret.returncode
            (sbatch_msg_stderr, sbatch_msg_stdout) = ("", "")
            stderr_file = open(local_cluster_stderr_file_path, "r")
            sbatch_msg_stderr = stderr_file.read()
            stderr_file.close()
            stdout_file = open(local_cluster_stdout_file_path, "r")
            sbatch_msg_stdout = stdout_file.read()
            stdout_file.close()

            (sbatch_error_case, sbatch_errormsg) = sbatch_control_out(
                                         retcode=retcode,
                                         sbatch_msg_stdout=sbatch_msg_stdout,
                                         sbatch_msg_stderr=sbatch_msg_stderr)
            if sbatch_error_case :
                msg = "error while remote muse launching " +sw_cmd
                msg += " into " +local_run_path+ " : "+sbatch_errormsg
                raise Exception(msg)

        except :
            raise

        return (retcode, sbatch_msg_stdout, sbatch_msg_stderr)

