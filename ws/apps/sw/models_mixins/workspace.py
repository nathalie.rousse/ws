"""sw.models_mixins.workspace
  
Workspace (local paths depending on calcul resource)

"""

import os
import shutil

from cmn.utils.folder_file import get_available_pathname
from cmn.utils.folder_file import create_dir_if_absent
from cmn.utils.folder_file import make_zip_folder

from cmn.utils.coding import json_str_from_py

from conf.config import WS_STDOUT_FILENAME
from conf.config import WS_STDERR_FILENAME
from conf.config import WS_CMD_FILENAME
from conf.config import WS_REPORT_FILENAME


class RunWorkspace(object) :
    """RunWorkspace has only run_path with its existing folder"""

    def __init__(self, run_path) :
        self.run_path = run_path

    @classmethod
    def get_run_workspace(cls, run_path) :
        run_workspace = RunWorkspace(run_path=run_path)
        return run_workspace

    def get_run_path(self) :
        return self.run_path

    @classmethod
    def define_stdout_file_path(cls, path) :
        return os.path.join(path, WS_STDOUT_FILENAME)

    def get_stdout_file_path(self) :
        return self.define_stdout_file_path(self.run_path)

    @classmethod
    def define_stderr_file_path(cls, path) :
        return os.path.join(path, WS_STDERR_FILENAME)

    def get_stderr_file_path(self) :
        return self.define_stderr_file_path(self.run_path)

    def get_cmd_file_path(self) :
        return os.path.join(self.run_path, WS_CMD_FILENAME)

    @classmethod
    def define_report_file_path(cls, path) :
        return os.path.join(path, WS_REPORT_FILENAME)

    def get_report_file_path(self) :
        return self.define_report_file_path(self.run_path)

    def make_report_file(self, report) :
        report_file_path = self.get_report_file_path()
        f = open(report_file_path, "w")
        f.write(json_str_from_py(report))
        f.close()

    def make_run_zip(self, report=None) :
        if report is not None : 
            self.make_report_file(report=report)
        zip_folder_path = make_zip_folder(folderpath=self.run_path)
        return zip_folder_path


class Workspace(RunWorkspace, object) :

    def __init__(self) :

        self.home_path = None   # default

        # all other possibilities (that will maybe not be used)
        self.run_path = None    # default
        self.data_path = None   # default

    @classmethod
    def create(cls, rsrc) :
        """Creates and initializes a workspace about rsrc resource

        home_path is defined and created

        home_path subdirectories are declared here. They will later be used
        or not (depending on use cases). They will be defined and created
        if/when used.
        """

        workspace = Workspace()

        home_path = get_available_pathname(
                     rootpath=rsrc.get_local_requests_home(), rootname="user_")
        creation_done = create_dir_if_absent(dirpath=home_path)
        if not creation_done :
            msg = "unable to create home directory "+home_path
            msg =+ " for workspace"
            raise Exception(msg)
        workspace.home_path = home_path
        return workspace

    def get_home_path(self) :
        return self.home_path

    def define_run_path(self) :
        return os.path.join(self.home_path, "run")

    def define_data_path(self) :
        return os.path.join(self.home_path, "data")

    def get_data_path(self) :
        return self.data_path

    def make_run_path(self) :
        """Creates run folder under home_path and returns its value"""

        run_path = self.define_run_path()
        creation_done = create_dir_if_absent(dirpath=run_path)
        if not creation_done :
            msg = "unable to create run subdirectory "+run_path
            msg =+ " for workspace"
            raise Exception(msg)
        self.run_path = run_path
        return run_path

    def get_remote_stdout_file_path(self, remote_run_path) :
        return self.define_stdout_file_path(remote_run_path)

    def get_remote_stderr_file_path(self, remote_run_path) :
        return self.define_stderr_file_path(remote_run_path)

