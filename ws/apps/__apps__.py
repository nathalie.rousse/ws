"""ws.apps

*This package is part of ws web services*

:copyright: Copyright (C) 2020-2024 INRAE http://www.inrae.fr
:license: GPLv3, see LICENSE file for more details
:authors: see AUTHORS file

django applications of the ws package.

django applications are subdirectories of apps.

The 'apps' path must be included into PYTHONPATH.

"""

