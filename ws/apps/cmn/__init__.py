"""cmn
  
*This package is part of ws web services*

:copyright: Copyright (C) 2020-2024 INRAE http://www.inrae.fr
:license: GPLv3, see LICENSE file for more details
:authors: see AUTHORS file

Common application
 
The cmn common application contains resources for other applications.

"""

