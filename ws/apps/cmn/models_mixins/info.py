"""cmn.models_mixins.info

Mixins about info field of some models

info : data information (JSON value as str)

"""

from cmn.utils.coding import json_str_from_py
from cmn.utils.coding import json_py_from_str

class InfoMixin(object):
    """Additional methods for a model having an 'info' field

    info : data information (JSON value as str)
    """

    def info_update_and_save_from(self, I) :
        """Updates info from I (python type) and saves it into the database """

        self.info = json_str_from_py(I)
        self.save()

    def extract_from_info(self) :
        """Extracts and returns python type from info (JSON value as str)"""

        I = json_py_from_str(self.info)
        return I

