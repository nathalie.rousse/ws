"""cmn.models_mixins.priv

Mixins about priv field of some models

priv : private information (JSON value as str)

"""

from cmn.utils.coding import json_str_from_py
from cmn.utils.coding import json_py_from_str

class PrivMixin(object):
    """Additional methods for a model having a 'priv' field

    priv : private information (JSON value as str)
    """

    def priv_update_and_save_from(self, P) :
        """Updates priv from P (python type) and saves it into the database """

        self.priv = json_str_from_py(P)
        self.save()

    def extract_from_priv(self) :
        """Extracts and returns python type from priv (JSON value as str) """

        P = json_py_from_str(self.priv)
        return P

