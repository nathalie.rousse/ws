"""cmn.using.send_get_and_receive

Methods that may be used by a user calling the ws web services from python

"""

import urllib.parse as urllib_local

import pycurl
import io
import json

def send_get_and_receive(url, p=None, options=None):
    """Sends GET request and return response datas

       full_url composed by : url {p} /? options
       p as a string
    """

    print("send_get_and_receive BEGIN")

    full_url = url
    if p is not None :
        full_url = full_url + "%s/" % p
    if options is not None :
        full_url = full_url + "?%s" % urllib_local.urlencode(options)
    print("request GET ", full_url, "\n")
    buffer = io.BytesIO()
    c = pycurl.Curl()
    c.setopt(c.CUSTOMREQUEST, 'GET')
    c.setopt(c.URL, full_url)
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    responsedata = json.loads(buffer_str)
    #print("responsedata :", responsedata)
    return responsedata

