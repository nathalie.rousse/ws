"""cmn.forms

Forms common to ws applications

"""

from django import forms

class ReadonlyForm(forms.ModelForm):
    """Form with all fields readonly and in textarea"""

    def __init__(self, *args, **kwargs):
        super(ReadonlyForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            f.widget=forms.Textarea(attrs={'rows':2, 'cols':100})
            f.widget.attrs['readonly'] = True

class TextAreasForm(forms.ModelForm):
    """Form with all fields in textarea"""

    def __init__(self, *args, **kwargs):
        super(TextAreasForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            f.widget=forms.Textarea(attrs={'rows':2, 'cols':100})

    def make_readonly(self, name_field):
        f = self.fields[name_field]
        f.widget.attrs['readonly'] = True

