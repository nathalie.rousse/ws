"""cmn.views_mixins

Mixins common to ws applications views

"""

from django.http import FileResponse
from django.http import HttpResponse
from wsgiref.util import FileWrapper

from rest_framework.response import Response

from rest_framework.negotiation import DefaultContentNegotiation

from cmn.serializers import FormatSerializer

from cmn.utils.folder_file import is_binary_file
#from cmn.utils.folder_file import is_txt_file
from cmn.utils.folder_file import is_csv_file

from cmn.utils.errors import complete_error_message

class UnavailableRequestViewMixin(object):
    """Additional methods for views having some unavailable requests"""

    def unavailable_request(self, error_message, more_message) :
        """Builds and returns response to an unavailable request

        Returns error_message and more_message into error message 
        """

        context = dict()
        msg = "Unable to satisfy the request"
        errormsg = complete_error_message(error_message, msg)
        context = {'error':errormsg, 'more':more_message}
        response = Response(context)
        return response

class DataViewMixin(object):
    """Additional methods for views needing data"""

    def request_query_params(self, request):
        return request.query_params

    def request_data(self, request):
        return request.data

    def request_data_writable(self, request):
        """Allows request.data modification """
        request.data._mutable = True

    def request_data_unwritable(self, request):
        """Disallows request.data modification """
        request.data._mutable = False

    def data_from_request(self, request):
        if request.method == 'POST' :
            data = self.request_data(request)
        else : # 'GET'
            data = self.request_query_params(request)
        return data

class FormatViewMixin(object):
    """Additional methods for views having 'format' option """

    def get_format_value(self, data):
        """Returns the 'format' value

        Reads and adapts the 'format' value found in data 

        Available values : 'json', 'api', 'yaml', 'xml'

        Note:
        problem with 'yaml' and 'xml' (...import six...)

        """

        p = FormatSerializer(data=data)
        p.is_valid()
        format = p.data['format']

        default_format = 'json'
        if format is not None :
            if (format=='json') or (format=='api') :
                pass
            else : # 'yaml', 'xml' or else
                format = default_format
        return format

class FormatContentNegotiation(FormatViewMixin, DataViewMixin,
                               DefaultContentNegotiation):
    """ContentNegotiation based on 'format' as GET/POST parameter """

    def select_parser(self, request, parsers):
        return super(FormatContentNegotiation, self).select_parser(request,
                                                                   parsers)

    def select_renderer(self, request, renderers, format_suffix):
        data = self.data_from_request(request)
        format_suffix = self.get_format_value(data=data)
        return super(FormatContentNegotiation, self).select_renderer(request,
                                                     renderers, format_suffix)

class FileViewMixin(object) :
    """Additional methods for views returning files """

    def binaryfile_response(self, file_path):
        file = open(file_path, 'rb')
        response = FileResponse(file)
        return response

    def zipfile_response(self, file_path):
        return self.binaryfile_response(file_path)

    def textfile_response(self, file_path):

        def file_content_type(file_path) :
            content_type = 'text/plain' # default
            if is_csv_file(file_path) :
                content_type = "text/csv"
            #elif is_txt_file(file_path) :
            #    content_type = 'text/plain'
            #elif ... __todo__ to be completed ?
            return content_type

        wrapper = FileWrapper(open(file_path, 'r'))
        content_type = file_content_type(file_path)
        content_disposition = "attachment; filename="+file_path+"'"
        response = HttpResponse(wrapper, content_type=content_type)
        response['Content-Disposition'] = content_disposition
        return response

    def txtfile_response(self, file_path):
        return self.textfile_response(file_path)

    def file_response(self, file_path):
        if is_binary_file(file_path) :
            response = self.binaryfile_response(file_path=file_path)
        else :
            response = self.textfile_response(file_path=file_path)
        return response

