"""cmn.utils.log

Methods to get/record logs

"""

import datetime

from conf.config import LOG_ACTIVE
from conf.config import LOG_FILE

def to_log(request) :

    if LOG_ACTIVE :
        try :
            text = str(datetime.datetime.now())
            text += " -R- " + request.method +" "+ request.path
            if request.method == 'POST' :
                data = request.data
            else : # 'GET'
                data = request.query_params
            text += " -D- " + str(data) + "\n"
            f = open(LOG_FILE, "a")
            f.write(text)
            f.close()
        except :
            pass

