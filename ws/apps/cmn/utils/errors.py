"""cmn.utils.errors
  
Methods to manage errors

"""

def complete_error_message(errormsg, msg) :
    """Returns message error completed by msg if not already into """

    if msg in errormsg :
        return errormsg
    elif errormsg == "" :
        return (msg + errormsg)
    else :
        return (msg + " -- " + errormsg)

def get_exception_info(exception) :
    """Returns exception information"""

    errortype = type(exception).__name__
    errordetails = exception.args
    return (errortype, errordetails)

def get_msg_not_implemented_method(c, m) :
    msg = m+" method not implemented into "+c+" class !!!"
    return msg

class Http403(Exception):
    pass

