"""cmn.utils.urls

Methods about urls

"""

import urllib.parse
def url_add_options(path, options):
    return path + '?' + urllib.parse.urlencode(options)

def get_host_name(request) :
    return request.get_host().split(":")[0]

def get_absolute_url(root_url, relative_url) :
    return (root_url + '/' + relative_url)

