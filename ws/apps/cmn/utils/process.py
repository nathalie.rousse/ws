"""cmn.utils.process

Subprocess methods

"""

import os
import subprocess

def remote_call_subprocess_str_mode(remote_host, remote_cmd) :
    """Runs remote_cmd (text) as subprocess with connection to remote_host 

    Returns the subprocess returned code (retcode), stdout and stderr messages
    """

    try :
        ret = subprocess.run(["ssh", remote_host, remote_cmd], shell=False,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                             check=False)
        retcode = ret.returncode
        msg_stdout = ret.stdout
        msg_stderr = ret.stderr
    except :
        raise
    return (retcode, msg_stdout, msg_stderr)

def remote_call_subprocess_file_mode(remote_host, remote_cmd, local_run_path,
                              local_stderr_file_path, local_stdout_file_path,
                              local_cmd_file_path) :
    """Runs remote_cmd (text) as subprocess with connection to remote_host 

    Returns the subprocess returned code (retcode),
    produces local_stdout_file_path and local_stderr_file_path files and also
    local_cmd_file_path file.
    """

    try :
        f = open(local_cmd_file_path, 'w')
        f.write(remote_cmd)
        f.close()
        stderr = open(local_stderr_file_path,'w')
        stdout = open(local_stdout_file_path,'w')

        retcode = subprocess.run(["ssh", remote_host, remote_cmd], shell=False,
                                 stdout=stdout, stderr=stderr, check=False)
        stderr.close()
        stdout.close()
    except :
        raise
    return retcode

def local_call_subprocess(cmd, stderr_file_path, stdout_file_path,
                          cmd_file_path) :
    """Runs cmd (text) as subprocess 

    Returns the subprocess returned code (retcode), produces stdout and stderr 
    files.

    Note : also produces cmd file
    """
    try :
        f = open(cmd_file_path, 'w')
        f.write(cmd)
        f.close()
        stderr = open(stderr_file_path,'w')
        stdout = open(stdout_file_path,'w')
        retcode = subprocess.call(cmd, shell=True,
                                  stderr=stderr, stdout=stdout)
        stderr.close()
        stdout.close()
    except :
        raise
    return retcode

