"""cmn.utils.folder_file

Methods on folders and files

"""

import os
import stat
import shutil
import pathlib
import zipfile
import csv
import uuid

def is_a_directory(path):
    """Returns True if path exists as a directory and else returns False """

    if os.path.exists(path):
        if os.path.isdir(path):
            return True
    return False

def is_a_file(path):
    """Returns True if path exists as a file and else returns False """

    if os.path.exists(path):
        if os.path.isfile(path):
            return True
    return False

def create_dir_if_absent(dirpath):
    """Creates the directory dirpath if dirpath does not yet exist.

    Returns True if the creation has been done.
    """

    creation_done = False
    if not os.path.exists(dirpath):
        try:
            os.mkdir(dirpath)
            creation_done = True
        except Exception as e :
            msg = "unable to create directory "+dirpath
            raise Exception(msg)
        return creation_done

def delete_path_if_present(path):
    """Deletes the path if it exists.

    Returns True if the deletion has been done.
    """

    deletion_done = False
    if os.path.exists(path):
        try:
            if os.path.isfile(path):
                os.remove(path)
                deletion_done = True
            elif os.path.isdir(path):
                shutil.rmtree(path)
                deletion_done = True
        except Exception as e :
            msg = "unable to delete "+dirpath
            raise Exception(msg)
    return deletion_done

def clean_dir(dirpath):
    """Creates the directory dirpath after having deleting dirpath if still
    existing.
    """

    deletion_done = delete_path_if_present(dirpath)
    creation_done = create_dir_if_absent(dirpath)
    return (deletion_done, creation_done)

def set_all_user_rights(file_path):
    os.chmod(file_path, stat.S_IRWXU)

def get_txt_from_file(file_path) :
    """Reads txt into file and returns it"""

    f = open(file_path, "r")
    txt = f.read()
    f.close()
    return txt

def make_csv_file(row_list, filename, dirpath, delimiter=";"):
    """Builds a csv file from row_list and saves it as dirpath/filename """

    csvfile = os.path.join(dirpath, filename)
    file = open(csvfile, "w")
    c = csv.writer(file, delimiter=delimiter)
    for row in row_list:
        c.writerow(row)
    file.close()

def is_txt_file(file_path) :
    return (pathlib.Path(file_path).suffix == ".txt")

def is_csv_file(file_path) :
    return (pathlib.Path(file_path).suffix == ".csv")

def is_zip_file(file_path) :
    return zipfile.is_zipfile(file_path)

def is_binary_file(file_path) :
    with open(file_path, 'rb') as f:
        if b'\x00' in f.read():
            return True
        else:
            return False

def make_zip_folder(folderpath):
    """Builds the zip file of the folderpath
        
    Returns the zip file path name.
    """

    format = 'zip'
    shutil.make_archive( folderpath, format, folderpath)
    zip_path = folderpath+'.'+format
    return zip_path

def make_zip_folder2(folderpath):
    """Builds the zip file of the folderpath
        
    Returns the zip file path name.
    """

    format = 'zip'
    shutil.make_archive(folderpath, format, '.', folderpath)
    zip_path = folderpath+'.'+format
    return zip_path

def unzip_file(zip_file_path, path):
    """Unzip zip_file_path into path.
        
    Returns True if success, and else False.
    """

    cr_ok = False
    try :
        zip_file = zipfile.ZipFile(file=zip_file_path)
        zip_file.extractall(path=path)
        cr_ok = True
    except :
        raise
    return cr_ok

def save_uploaded_file(imulf, file_path) :
    """Saves into the file_path file an uploaded file 

    imulf is an in memory uploaded file
    (type django.core.files.uploadedfile.InMemoryUploadedFile)
    imulf has been sent in POST data

    __wsdev__ Note : exploit the following information (?) :
              type(imulf) , imulf.name , imulf.size
              imulf.content_type , imulf.content_type_extra imulf.charset
    """

    cr_ok = False
    try :
        with open(file_path, "wb") as f:
            for chunk in imulf.chunks():
                f.write(chunk)
            f.close()
        cr_ok = True
    except :
        raise
    return cr_ok

def get_available_pathname(rootpath, rootname):
    """Defines and returns a path name that doesn't yet exist.

    The path is of a rootpath subdirectory, with name based on rootname.

    Note : The pathname directory should be immediately created !
    """

    found = False
    while not found:
        pseudoid = str(uuid.uuid4())
        name = rootname + pseudoid
        pathname = os.path.join(rootpath, name)
        if not os.path.exists(pathname):
            found = True
    return pathname

def recursive_overwrite(src, dest):
    """Recursive overwrite taking into account directories and files

    Links are not taken into account
    """

    try :
        if os.path.isdir(src):
            if not os.path.isdir(dest):
                os.makedirs(dest)
            files = os.listdir(src)
            for f in files:
                recursive_overwrite(os.path.join(src, f),
                                    os.path.join(dest, f))
        else:
            shutil.copyfile(src, dest)
    except :
        raise

