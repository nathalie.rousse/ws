"""cmn.utils.coding

Coding methods

"""

import json

def json_str_from_py(v) :
    """Returns JSON value as str (from v of python type) """

    return json.dumps(v)

def json_py_from_str(v) :
    """Returns JSON value as python type (from str v)

    Returns None if v values ''
    """

    if v == "" :
        return None
    else : 
        return json.loads(v)

