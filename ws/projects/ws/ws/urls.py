"""ws.projects.ws.ws.urls

ws URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include

#from rest_framework_jwt.views import obtain_jwt_token
#from rest_framework_jwt.views import verify_jwt_token

from conf.config import IN_PRODUCTION

from cmn import views as cmn_views

if IN_PRODUCTION :
    PRE = '' # see WSGIScriptAlias into ws.conf
else :
    PRE = 'api/'

# admin
urlpatterns = [
    path(PRE+'admin/', admin.site.urls, name='admin'),
]

# __todo__
# JWT Authentication, urls kept whereas replaced by acs urls
# (see acs/jwt/obtain/, acs/jwt/verify/, acs/login)
#urlpatterns += [
#    path('api-token-auth/', obtain_jwt_token, name="api-token-auth"),
#   #path('api-token-refresh/', refresh_jwt_token),
#    path('api-token-verify/', verify_jwt_token, name="api-token-verify"),
#]
#urlpatterns += [
#    path('api/login', obtain_jwt_token),
#]

urlpatterns += [

    # software (main)
    path(PRE, include('sw.urls')),

    # load (downloading files)
    path(PRE, include('load.urls')),

    # access (different levels)
    path(PRE, include('acs.urls')),


    # tools (based on ws)
    path(PRE, include('tools.urls')),

]

