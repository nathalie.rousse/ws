"""ws.projects.ws.ws

*This package is part of ws web services*

:copyright: Copyright (C) 2020-2024 INRAE http://www.inrae.fr
:license: GPLv3, see LICENSE file for more details
:authors: see AUTHORS file

Django project Python package of web services

"""

