# ws web services


ws
== 

The ws web services allow to run some softwares that have previously been
delivered and installed as required.

How to install
============== 

- Install ws web services : see ws/ws/install

- Install some softwares to be run by ws web services : see ws/softwares/install


How to use
==========

See doc/source/api

