"""build_software_default_doc"""

#import sys
#apps_home = "/opt/ws/ws/apps"
#if apps_home not in sys.path :
#    sys.path.insert(0, apps_home)

SOFTWARES_DOC_HOME_PATH = "/opt/ws/fab/softwares/default_doc"

DEFAULT_INDEX_FILENAME = "sw_index.html"

BASE_URL = "http://127.0.0.1:8000/api/"
#BASE_URL = "http://147.100.179.250/api/"

import os
from cmn.utils.folder_file import create_dir_if_absent
from cmn.utils.folder_file import delete_path_if_present
from cmn.using.send_get_and_receive import send_get_and_receive

def get_html_txt(software_data) :
    """ Builds (from software_data) and returns html code text"""

    software_data_keys = software_data.keys()

    name = "(Error:undefined)"
    if 'name' in software_data_keys :
        name = software_data['name']

    def b(balise,txt) :
        return ("<"+balise+">"+txt+"</"+balise+">")

    def vb(k) :
        if k == 'name':
            return 'Name'
        elif k == 'info': 
            return 'Information'
        elif k == 'sw':
            return 'Software'
        elif k == 'desc': 
            return 'Description'
        elif k == 'shortname': 
            return 'Shortname'
        elif k == 'urls': 
            return 'URLs'
        elif k == 'cont': 
            return 'Container'
        elif k == 'type': 
            return 'Type'
        elif k == 'image': 
            return 'Image'
        else :
            return k

    txt=     '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '
    txt=txt+ '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
    txt=txt+ '' + "\n"
    txt=txt+ '<html xmlns="http://www.w3.org/1999/xhtml">' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<head>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<meta http-equiv="Content-Type" '
    txt=txt+ ' content="text/xhtml;charset=UTF-8"/>' + "\n"
    txt=txt+ '<title>' + name + ' / ws </title>' + "\n"
    txt=txt+ '<link href="tabs.css" rel="stylesheet" type="text/css"/>' + "\n"
    txt=txt+ '<link href="doxygen.css" rel="stylesheet" type="text/css"/>'
    txt=txt+ "\n"
    txt=txt+ '<style type="text/css">' + "\n"
    txt=txt+ '  body,div,table,thead,tbody,tfoot,tr,th,td,p '
    txt=txt+ '  { font-family:"Times New Roman"; font-size:medium }'
    txt=txt+ "\n"
    txt=txt+ '  table { border-collapse: collapse; }' + "\n"
    txt=txt+ '  table, td, th { border: 1px solid black; text-align: left; '
    txt=txt+ '  font-weight: normal; padding: 8px; }'
    txt=txt+ '' + "\n"
    txt=txt+ '</style>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '</head>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<body>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '<div style="background-color:WhiteSmoke">' + "\n"
    txt=txt+ '<br>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '<h1 id="name" align=center>Software ' + name + ' </h1>' + "\n"

    if 'info' in software_data_keys :
        info = software_data['info']

        for k in info.keys() : 

            sous_info = info[k]
            if type(sous_info) is dict :
                txt=txt+ '' + "\n"
                txt=txt+ b("h2",vb(k)) + "\n"
                txt=txt+ '' + "\n"
                for sk in sous_info.keys() :
                    sous_sous_info = sous_info[sk]
                    if type(sous_sous_info) is dict :
                        txt=txt+ b("h3",vb(sk)) + "\n"
                        txt=txt+ '' + "\n"
                        txt=txt+ '<ul>' + "\n"
                        for ssk in sous_sous_info.keys() :
                           txt=txt+ '<li>'
                           if vb(sk) == "URLs" : # special "URLs" case
                               txt=txt+ b("b",vb(ssk))+' : '
                               txt=txt+ '<a href='+sous_sous_info[ssk]+'>'
                               txt=txt+ sous_sous_info[ssk]+'</a>'
                           else :
                               txt=txt+ b("b",vb(ssk))+' : '+sous_sous_info[ssk]
                           txt=txt+ '</li>' + "\n"
                        txt=txt+ '</ul>' + "\n"
                    else :
                       txt=txt+ '<ul><li>'
                       txt=txt+ b("p", b("b",(vb(sk)))+' : '+str(sous_sous_info))
                       txt=txt+ '</li></ul>' + "\n"
            else :
                txt=txt+ '' + "\n"
                txt=txt+ '<ul><li>'
                txt=txt+ b("p",b("b",vb(k))+' : '+sous_info)
                txt=txt+ '</li></ul>' + "\n"

    small_txt = name + " / ws"
    txt=txt+ '' + "\n"
    txt=txt+ '<br>' + "\n"
    txt=txt+ '</div>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<hr size="1"/><address style="text-align: right;"><small>'
    txt=txt+ small_txt + '</small></address>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt + "<!-- "+ "no comment" + " -->" + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '</body>' + "\n"
    txt=txt+ '</html>' + "\n"

    return txt

def build_html_home_page(software_name,
                         softwares_doc_home_path=SOFTWARES_DOC_HOME_PATH,
                         default_index_filename=DEFAULT_INDEX_FILENAME,
                         base_url=BASE_URL) :
    """Builds default html index page for a software of ws database

    Software named software_name

    Creates or updates default_index_filename under
    folder sw_doc_path = os.path.join(softwares_doc_home_path, software_name)
    
    __todo__ : default_index_filename rights : R, X?, no W

    """

    create_dir_if_absent(softwares_doc_home_path)

    sw_doc_path = os.path.join(softwares_doc_home_path, software_name)
    default_index_file_path = os.path.join(sw_doc_path, default_index_filename)
    create_dir_if_absent(sw_doc_path)
    delete_path_if_present(default_index_file_path)

    # GET software detail name
    url = base_url + "software/detail/"
    options = {'format':'json'}
    software_data = send_get_and_receive(url=url, p=software_name,
                                         options=options)

    file = open(default_index_file_path, "w")
    txt = get_html_txt(software_data)
    file.write(txt)
    file.close()

###############################################################################
#                                  MAIN
###############################################################################
#
#print(sys.argv, " command")
#
#if len(sys.argv) >= 2 :
#    software_name = sys.argv[1]
#    print("software_name : ", software_name)
#else : 
#    print("software name missing")
#
#build_html_home_page(software_name=software_name)
#
###############################################################################

