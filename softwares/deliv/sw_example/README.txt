###############################################################################
#                          Software delivery
###############################################################################

Software source code
====================

software source code : src/add.py

  * The command "python add.py p1 p2" returns (p1+p2) value.

  * The command "python add.py",
    with stdin.txt file containing : p1
                                     p2
    returns (p1+p2) value.

###############################################################################

Software delivered as docker image
==================================

how_to_build
------------

how_to_build sw_example-docker image :

  docker build -t sw_example-docker .

how_to_use
----------

how_to_use sw_example :

  - Run add.py into sw_example-docker image (for p1=4 and p2=5) :

    docker run -t sw_example-docker python /add.py 4 5 > stdout.txt

For more : see Dockerfile

###############################################################################

Software delivered as singularity image
=======================================

how_to_build
------------

how_to_build sw_example-singularity.simg image (from sw_example-docker) :

  - Build sw_example-docker : see above.

  - to control docker image identity : 

    docker image ls

        REPOSITORY         TAG         IMAGE ID      CREATED        SIZE
        sw_example-docker  latest      3946ce5c32bc  2 minutes ago  178MB

  - Build sw_example-singularity.simg :

    sudo singularity build sw_example-singularity.simg docker-daemon://sw_example-docker:latest

how_to_use
----------

how_to_use sw_example :

  - Run add.py into sw_example-docker image (for p1=4 and p2=5) :

    singularity exec sw_example-singularity.simg python /add.py 4 5 > stdout.txt

  - Run add.py into sw_example-docker image (giving p1 and p2 into stdin.txt) :

    with stdin.txt containing p1 and p2, 
    with usrcmd.sh a shell script file managing WS folder and containing
    command : python /add.py

    singularity exec --no-home --bind $PWD:/WS sw_example-singularity.simg /WS/usrcmd.sh > stdout.txt

###############################################################################

