###############################################################################
# 
#              Software add.py
# 
# Call command : "python add.py p1 p2" returns p1+p2 value 
#
# Call command : "python add.py" returns p1+p2 value, with
# p1 and p2 given into 1st and 2nd lines of file named stdin.txt.
#
###############################################################################

import sys

def addition(a, b) :
    return (a + b)

if len(sys.argv) >= 3 :
    p1 = float(sys.argv[1])
    p2 = float(sys.argv[2])
    ret = addition(p1, p2)
elif len(sys.argv) == 1 :
    f = open("stdin.txt", "r")
    p1 = float(f.readline())
    p2 = float(f.readline())
    f.close()
    ret = addition(p1, p2)
else :
    ret = "parameters a, b missing"

print(ret)

###############################################################################

