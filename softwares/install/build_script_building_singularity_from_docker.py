"""build_script_building_singularity_from_docker.py"""

#import sys
#apps_home = "/opt/ws/ws/apps"
#if apps_home not in sys.path :
#    sys.path.insert(0, apps_home)

import os

from conf.config import INSTALL_HOME_FAB

from cmn.utils.folder_file import set_all_user_rights
from cmn.utils.folder_file import create_dir_if_absent
from cmn.utils.folder_file import delete_path_if_present
from cmn.using.send_get_and_receive import send_get_and_receive

SCRIPT_FOLDER_PATH = INSTALL_HOME_FAB
SCRIPT_FILENAME = "build_singularity_from_docker.sh"
script_file_path = os.path.join(SCRIPT_FOLDER_PATH, SCRIPT_FILENAME)

from conf.config import WSM_LOCAL_SIMG_HOME
SIMG_HOME = WSM_LOCAL_SIMG_HOME

BASE_URL = "http://127.0.0.1:8000/api/"

def get_script_file_txt(container_list) :
    """ Builds (from container_list) and returns code text"""

    txt =  "#!/bin/bash" + "\n"
    txt +=  "\n"
    txt +=  "\n"

    for container in container_list :
        ocn_type = container["type"]
        ocn_image = container["image"]
        if ocn_type == "docker" :
            docker_image = ocn_image
            singularity_file_name = "."+docker_image+".simg"
            singularity_file_path = os.path.join(SIMG_HOME,
                                                 singularity_file_name)
            txt += "sudo singularity build -F "
            txt += singularity_file_path
            txt += " docker-daemon://"+docker_image + "\n"
    txt +=  "\n"
    txt += "# Memo" + "\n"
    txt += "# Command to identify docker images (REPOSITORY and TAG) :" + "\n"
    txt += "# docker image ls" + "\n"
    return txt

def generate_script_file(script_file_path=script_file_path, base_url=BASE_URL) :
    """Generates the script file building singularity from docker

    The generated script file builds, for each container of ws database with
    type=docker, its singularity image file from its docker image.

    With a docker container with its image named docker_image_name,
    creates or updates (enforcing) the .'docker_image_name'.simg file
    under SIMG_HOME folder.

    """

    # GET container list
    url = base_url + "container/list/"
    options = {'format':'json'}
    container_list = send_get_and_receive(url=url, options=options)

    create_dir_if_absent(SCRIPT_FOLDER_PATH)
    delete_path_if_present(script_file_path)

    file = open(script_file_path, "w")
    txt = get_script_file_txt(container_list)
    file.write(txt)
    file.close()
    set_all_user_rights(script_file_path)

###############################################################################
#                                  MAIN
###############################################################################
#
#print("\nGeneration of the script file", script_file_path, "...", "\n")
#
#generate_script_file()
#
#print("\nThe", script_file_path, "file has been generated")
#print("\nFollowing step :", "execute", script_file_path,
#      "script file (containing sudo)", "\n")
#
###############################################################################

